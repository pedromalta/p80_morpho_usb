// The present software is not subject to the US Export Administration Regulations (no exportation license required), May 2012
package com.morpho.morphosample;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import morpho.msosecu.sdk.api.MsoSecu;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;

import com.morpho.android.usb.USBManager;
import com.morpho.morphosample.database.DatabaseArrayAdapter;
import com.morpho.morphosample.database.DatabaseItem;
import com.morpho.morphosample.info.EnrollInfo;
import com.morpho.morphosample.info.FingerPrintInfo;
import com.morpho.morphosample.info.IdentifyInfo;
import com.morpho.morphosample.info.MorphoInfo;
import com.morpho.morphosample.info.ProcessInfo;
import com.morpho.morphosample.info.VerifyInfo;
import com.morpho.morphosample.info.subtype.SecurityOption;
import com.morpho.morphosample.info.subtype.SensorWindowPosition;
import com.morpho.morphosample.tools.DeviceDetectionMode;
import com.morpho.morphosample.tools.MorphoTools;
import com.morpho.morphosmart.sdk.Coder;
import com.morpho.morphosmart.sdk.DescriptorID;
import com.morpho.morphosmart.sdk.ErrorCodes;
import com.morpho.morphosmart.sdk.FieldAttribute;
import com.morpho.morphosmart.sdk.IMsoSecu;
import com.morpho.morphosmart.sdk.ITemplateType;
import com.morpho.morphosmart.sdk.MatchingStrategy;
import com.morpho.morphosmart.sdk.MorphoDevice.MorphoDevicePrivacyModeDBProcessingChoice;
import com.morpho.morphosmart.sdk.MorphoKCVID;
import com.morpho.morphosmart.sdk.StrategyAcquisitionMode;
import com.morpho.morphosmart.sdk.MorphoDatabase;
import com.morpho.morphosmart.sdk.MorphoDevice;
import com.morpho.morphosmart.sdk.MorphoField;
import com.morpho.morphosmart.sdk.MorphoTypeDeletion;
import com.morpho.morphosmart.sdk.MorphoUser;
import com.morpho.morphosmart.sdk.MorphoUserList;
import com.morpho.morphosmart.sdk.ResultMatching;
import com.morpho.morphosmart.sdk.SecuConfig;
import com.morpho.morphosmart.sdk.SecurityLevel;
import com.morpho.morphosmart.sdk.Template;
import com.morpho.morphosmart.sdk.TemplateFVP;
import com.morpho.morphosmart.sdk.TemplateFVPType;
import com.morpho.morphosmart.sdk.TemplateList;
import com.morpho.morphosmart.sdk.TemplateType;
import com.morpho.morphosmart.sdk.MorphoLogLevel;
import com.morpho.morphosmart.sdk.MorphoLogMode;
import com.morpho.morphosmart.sdk.MorphoDevice.MorphoDevicePrivacyModeStatus;
import com.rscja.deviceapi.UsbFingerprint;

@SuppressWarnings("deprecation")
@SuppressLint("UseValueOf")
public class MorphoSample extends TabActivity implements Observer
{
	static private String		captureTag							= "Capture";
	static private String		enrollTag							= "Enroll";
	static private String		verifyTag							= "Verify";
	static private String		fingerPrintTag						= "FingerPrint";
	static private String		identifyTag							= "Identify";

	static private int			secondPartTabIndex					= 0;

	private boolean				defaultWindowSensorPositionValue	= true;
	private boolean				defaultCoderChoiceValue				= true;
	private boolean				defaultSecurityLevelValue			= true;
	private boolean				defaultMatchingStrategyValue		= true;
	private boolean				defaultStrategyAcquisitionModeValue	= true;
	private Handler				mHandler							= new Handler();
	
	private boolean				defaultLogModeValue					= true;
	private boolean				defaultLogLevelValue				= true;

	static int					nbFile								= 0;
	static String				template1							= "";
	static String				template2							= "";
	static byte[]				current_key							= null;
	static byte[]				new_key								= null;
	static String				selected_file						= "";

	static private String		processTag							= "Process";
	static private int			PROCESS_TAB_INDEX					= 5;	
	public static boolean		isRebootSoft						= false;

	private ListView			databaseListView					= null;

	static MorphoDevice			morphoDevice						= null;
	static MorphoDatabase		morphoDatabase						= null;
	private MenuItem 			menuItemMSOConfiguration			= null;
	private MenuItem 			menuItemLoggingParameters			= null;
	private MenuItem 			menuItemuserAreaData				= null;
	private MenuItem 			menuItemLoadKsUnsecureMode			= null;
	private MenuItem 			menuItemLoadKsAsymetricSecureMode	= null;
	private MenuItem 			menuItemLoadKsSymetricSecureMode	= null;
	private MenuItem 			menuItemGetKs						= null;
	private MenuItem 			menuItemEnableBioDataEncryption		= null;
	private MenuItem 			menuItemDecryptBioData				= null;
	private MenuItem 			menuItemPrivacyMode					= null;
	private MenuItem 			menuItemLoadKprivacyUnsecureMode		= null;
	private MenuItem 			menuItemLoadKprivacySymmetricSecureMode	= null;
	private MenuItem 			menuItemGetprivacyKcv				= null;
	private int 				privacy_ret = 0;
	
	private MenuItem 			menuItemEncryptAESBioData			= null;
	private MenuItem 			menuItemDecryptAESBioData			= null;
	
	private IMsoSecu			msoSecu								= new MsoSecu();
	private boolean 			isOfferedSecurityMode 				= false;
	private boolean 			isTunnelingMode 					= false;
	private UsbDeviceConnection usbDeviceConnection					= null;
	
	private enum MorphoSampleAction	{
		onIdentityMatchClick,
		onVerifyMatchClick,
		onAddUserClick,
		onLoadKsUnsecureMode,
		onLoadKsAsymetricSecureMode,
		onLoadKsSymetricSecureMode,
		onDecryptBioData,
		onAesEncrypt,
		onAesDecrypt,
		onLoadKprivacyUnsecureMode,
		onLoadKprivacySymmetricSecureMode,
		onGetAesKeyLoad,
		onWriteUserAreaData
	}
	private MorphoSampleAction	currentAction						= MorphoSampleAction.onIdentityMatchClick;
	
	final static Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg)
		{
		    throw new RuntimeException();
		} 
	};
	
	synchronized int openDevice(int bus, int address, int fd) {
		int ret = ErrorCodes.MORPHO_OK;
		UsbManager usbManager = (UsbManager) this.getSystemService(Context.USB_SERVICE);
		HashMap<String, UsbDevice> usbDeviceList = usbManager.getDeviceList();

		Iterator<UsbDevice> usbDeviceIterator = usbDeviceList.values().iterator();
		while (usbDeviceIterator.hasNext()) {
			UsbDevice usbDevice = usbDeviceIterator.next();
			if (MorphoTools.isSupported(usbDevice.getVendorId(), usbDevice.getProductId())) {
				boolean hasPermission = usbManager.hasPermission(usbDevice);
				if (hasPermission) {
					usbDeviceConnection = usbManager.openDevice(usbDevice);
					if (usbDeviceConnection != null)
					{
						//Log.d("MORPHO_USB" ,"getting serial number .. #" + mConnection.getSerial());
						// int sensorFileDescriptor = connection.getFileDescriptor();
						String name = usbDevice.getDeviceName();

						String[] elts = name.split("/");
						if(elts.length < 5)
							continue;
						int sensorBus = Integer.parseInt(elts[4].toString());
						int sensorAddress = Integer.parseInt(elts[5].toString());
						//device fd can change on device reboot
						if(sensorBus == bus && sensorAddress == address /*&& sensorFileDescriptor == fd*/) {
							ret = morphoDevice.openUsbDeviceFD(bus, address, fd, 0);
							//Log.e("MORPHO_USB", "morphoDevice.openUsbDeviceFD " + ret);
						}
						break;
					}
				}
			}
		}

		return ret;
	}


	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		closeDeviceAndFinishActivity();
		UsbFingerprint.getInstance().UsbToHost();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_morpho_sample);
		USBManager.getInstance().initialize(this, "com.morpho.morphosample.USB_ACTION", true);

		morphoDevice						= new MorphoDevice();
		morphoDatabase						= new MorphoDatabase();
		if (ProcessInfo.getInstance().getMorphoDevice() == null)
		{
			String sensorName = ProcessInfo.getInstance().getMSOSerialNumber();
			int bus = ProcessInfo.getInstance().getMSOBus();
			int address = ProcessInfo.getInstance().getMSOAddress();
			int fd = ProcessInfo.getInstance().getMSOFD();
			DeviceDetectionMode detectionMode = ProcessInfo.getInstance().getMsoDetectionMode();
			int ret = ErrorCodes.MORPHO_OK;

			if (detectionMode == DeviceDetectionMode.SdkDetection) {
				ret = morphoDevice.openUsbDevice(sensorName, 0);
				Log.i("MORPHO_USB", "Opening device in DeviceDetectionMode.SdkDetection returns " + ret);
				
			} else if(detectionMode == DeviceDetectionMode.TCPDetection) {

				ret = morphoDevice.openDevicePipe(
						ProcessInfo.getInstance().getMsoPipeClient(),
						ProcessInfo.getInstance().getIp(),
						ProcessInfo.getInstance().getPort(),
						ProcessInfo.getInstance().getMSOSerialNumber(),
						ProcessInfo.getInstance().getTimeout()
					);
				Log.i("MORPHO_USB", "Opening device in DeviceDetectionMode.TCPDetection returns " + ret);
			} else {
				ret = openDevice(bus, address, fd);
				Log.i("MORPHO_USB", "Opening device in DeviceDetectionMode.UserDetection returns " + ret);
			}

			ArrayList<SecurityOption> securityOptions = ProcessInfo.getInstance().getSecurityOptions();
			for (SecurityOption so : securityOptions) {
				if (so.title.equals("Mode Offered Security")) {
					isOfferedSecurityMode = so.activated;
				} else if (so.title.equals("Mode Tunneling")) {
					isTunnelingMode = so.activated;
				}
			}
			
			if (isOfferedSecurityMode) {
				// Set OpenSSL path
				msoSecu.setOpenSSLPath(AppContext.RootPath+"Keys/");
				// Open Offered security mode
				ret = morphoDevice.offeredSecuOpen(msoSecu);
				Log.i("MORPHO_USB", "Opening device in Offered Security mode returns " + ret);
				if(ret != ErrorCodes.MORPHO_OK) {
					finish();
				}
			}
			
			if (isTunnelingMode) {
				// Set OpenSSL path
				msoSecu.setOpenSSLPath(AppContext.RootPath+"Keys/");
				// Get host certificate
				ArrayList<Byte> hostCertificate = new ArrayList<Byte>();
				msoSecu.getHostCertif(hostCertificate);
				// Open Tunneling mode
				ret = morphoDevice.tunnelingOpen(msoSecu, MorphoTools.toByteArray(hostCertificate));
				Log.i("MORPHO_USB", "Opening device in Tunneling mode returns " + ret);
				if(ret != ErrorCodes.MORPHO_OK) {
					finish();
				}
			}
			
		    MorphoDevicePrivacyModeStatus[] status = new MorphoDevicePrivacyModeStatus[1];
			privacy_ret = morphoDevice.getPrivacyModeStatus(status);
		    
		    if (ErrorCodes.MORPHO_OK != privacy_ret) {
				ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED);
			} else if (status[0] == MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED) {
				ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED);
			} else if (status[0] == MorphoDevicePrivacyModeStatus.PRIVACY_MODE_ENABLED) {
				ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_ENABLED);
			} else if (status[0] == MorphoDevicePrivacyModeStatus.PRIVACY_MODE_PARTIAL_ENABLED) {
				ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_PARTIAL_ENABLED);
			} else if (status[0] == MorphoDevicePrivacyModeStatus.PRIVACY_MODE_STANDALONE_ENABLED) {
				ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_STANDALONE_ENABLED);
			}
			
			if (ret == ErrorCodes.MORPHO_OK) {
				ret = morphoDevice.getDatabase(0, morphoDatabase);
				Log.i("MORPHO_USB", "morphoDevice.getDatabase = " + ret);
				if (ret != ErrorCodes.MORPHO_OK) {
					finish();
				}
			} else {
				finish();
			}
		}

		Button btn_CreeateDB = (Button) findViewById(R.id.btn_createbase);
		btn_CreeateDB.setEnabled(false);

		ProcessInfo.getInstance().setMorphoDevice(morphoDevice);
		ProcessInfo.getInstance().setMorphoDatabase(morphoDatabase);

		initDatabaseStatus();
		initNoCheck();
		loadDatabaseItem();
		initTabHost();
		initDatabaseInformations();
		initBioSettingsInformations();
		initOptionsInformations();
	
		switch (secondPartTabIndex)
		{
			case 0:
				onDatabaseInfoClick(null);
				break;
			case 1:
				onGeneralBioClick(null);
				break;
			case 2:
				onOptionsClick(null);
				break;
		}
		
		if(usbDeviceConnection != null){
			usbDeviceConnection.close();
			usbDeviceConnection=null;
		}


	}
	
	@Override
	protected void onResume() {
		super.onResume();
		enableDisableBoutton(false);
		morphoDevice.resumeConnection(30,MorphoSample.this);
	}

	@Override
	protected void onPause()
	{
		if (morphoDevice != null && ProcessInfo.getInstance().isStarted())
		{	
			stop(false);
//			try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
		}
		
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.activity_morpho_sample, menu);
		return true;
	}
	
	/**
	 * On options item selected.
	 * 
	 * @param item
	 *            the item
	 * @return true, if successful
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 * @since 1.0
	 */
	@Override
	public final boolean onOptionsItemSelected(final MenuItem item)
	{
		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		int itemId = item.getItemId();
		switch (itemId) {
			case R.id.msoconfiguration:
				SecuConfig secuConfig = new SecuConfig();
				int ret = morphoDevice.getSecuConfig(secuConfig);
				if (ret == 0) {
					String str_secuConfig;

					str_secuConfig = "\r\n" + morphoDevice.getSoftwareDescriptor() + "\r\n\r\n";

					str_secuConfig += "MSO License(s): " + morphoDevice.getStringDescriptorBin(DescriptorID.BINDESC_LICENSES) + "\r\n\r\n";
					
					str_secuConfig += "USB Daemon Version: " + USBManager.getInstance().getUsbDaemonVersion() + "\r\n\r\n";

					str_secuConfig += getResources().getString(R.string.msoserialnumber) + secuConfig.getSerialNumber() + "\r\n\r\n";
					str_secuConfig += getResources().getString(R.string.maxfar) + secuConfig.getSecurityFARDescription() + "\r\n\r\n";

					str_secuConfig += getResources().getString(R.string.securityoptions) + "\r\n";

					ArrayList<SecurityOption> securityOptions = ProcessInfo.getInstance().getSecurityOptions();
					for (SecurityOption so : securityOptions) {
						str_secuConfig += so.toString(getResources().getString(R.string.no), getResources().getString(R.string.yes)) + "\r\n";
					}

					alert(ret, 0, "Sensor Configuration", str_secuConfig);
				} else {
					alert(ret, morphoDevice.getInternalError(), "Sensor Configuration", "");
				}
				break;
			case R.id.sdkLoggingParameters:
				copyLogFileParam();
				LayoutInflater factory = LayoutInflater.from(this);
				final View logParamView = factory.inflate(R.layout.log_param, null);
										
				// Set spinner content from enum value for Log Level
				// -----------------------------------------------------------------------------------------
				Spinner spinnerLL = (Spinner) logParamView.findViewById(R.id.spinnerLoggingLevel);
				CharSequence[] itemLLArray = new CharSequence[MorphoLogLevel.values().length];
				int iCC = 0;
				for (MorphoLogLevel value : MorphoLogLevel.values())
				{
					itemLLArray[iCC++] = value.getLabel();
				}
				List<CharSequence> itemLLList = new ArrayList<CharSequence>(Arrays.asList(itemLLArray));
				ArrayAdapter<CharSequence> adapterLL = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item, itemLLList);
				adapterLL.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinnerLL.setAdapter(adapterLL);
				// set the default according to value
				int spinnerLLPosition = adapterLL.getPosition(ProcessInfo.getInstance().getLogLevel().getLabel());
				spinnerLL.setSelection(spinnerLLPosition);
				// listen to the event
				spinnerLL.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
				{
					public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
					{
						if (defaultLogLevelValue == true)
						{
							defaultLogLevelValue = false;
						}
						else
						{
							String item = (String) parent.getItemAtPosition(pos);
							ProcessInfo.getInstance().setLogLevel(MorphoLogLevel.fromString(item));
						}
					}

					public void onNothingSelected(AdapterView<?> parent)
					{
					}
				});
				
				// Set spinner content from enum value for Log Mode
				// -----------------------------------------------------------------------------------------
				Spinner spinnerLM = (Spinner) logParamView.findViewById(R.id.spinnerLoggingMode);
				CharSequence[] itemLMArray = new CharSequence[2];			
				itemLMArray[0] = MorphoLogMode.MORPHO_LOG_ENABLE.getLabel();
				itemLMArray[1] = MorphoLogMode.MORPHO_LOG_DISABLE.getLabel();
				
				List<CharSequence> itemLMList = new ArrayList<CharSequence>(Arrays.asList(itemLMArray));
				ArrayAdapter<CharSequence> adapterLM = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item, itemLMList);
				adapterLM.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spinnerLM.setAdapter(adapterLM);
				// set the default according to value
				int spinnerLMPosition = adapterLM.getPosition(ProcessInfo.getInstance().getLogMode().getLabel());
				spinnerLM.setSelection(spinnerLMPosition);
				// listen to the event
				spinnerLM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
				{
					public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
					{
						if (defaultLogModeValue == true)
						{
							defaultLogModeValue = false;
						}
						else
						{
							String item = (String) parent.getItemAtPosition(pos);
							ProcessInfo.getInstance().setLogMode(MorphoLogMode.fromString(item));
						}
					}

					public void onNothingSelected(AdapterView<?> parent)
					{
					}
				});
				
				alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
				alertDialog.setMessage("SDK Logging Parameters  : ");
				alertDialog.setCancelable(false);
				alertDialog.setView(logParamView);
				alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which)
					{				
					}
				});
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which)
					{
						int ret = morphoDevice.setLoggingMode(ProcessInfo.getInstance().getLogMode());

						if(ret != 0)
						{
							alert(ret,morphoDevice.getInternalError(),"SDK Logging Parameters","setLoggingMode");
						}
						else
						{					
							ret = morphoDevice.setLoggingLevelOfGroup(0,ProcessInfo.getInstance().getLogLevel());
							alert(ret,morphoDevice.getInternalError(),"SDK Logging Parameters","");
						}
					}
				});		
				alertDialog.show();
				break;
			case R.id.loadKsUnsecureMode:
				alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
				alertDialog.setMessage(getString(R.string.selectExternalKey));
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						currentAction = MorphoSampleAction.onLoadKsUnsecureMode;
						try {
							Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
							startActivityForResult(activityIntent, 0);
						} catch (Exception e) {
						}
					}
				});
				alertDialog.show();
				break;
			case R.id.loadKsAsymetricSecureMode:
				alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
				alertDialog.setMessage(getString(R.string.selectExternalKey));
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						currentAction = MorphoSampleAction.onLoadKsAsymetricSecureMode;
						try {
							Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
							startActivityForResult(activityIntent, 0);

						} catch (Exception e) {
						}
					}
				});
				alertDialog.show();
				break;
			case R.id.loadKsSymetricSecureMode:
				alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
				alertDialog.setMessage(getString(R.string.selectCurrentKey));
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						currentAction = MorphoSampleAction.onLoadKsSymetricSecureMode;
						try {
							Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
							startActivityForResult(activityIntent, 0);

						} catch (Exception e) {
						}
					}
				});
				alertDialog.show();
				break;
			case R.id.getKs:
				byte[] kcv = morphoDevice.getKCV(MorphoKCVID.ID_KS, null);
				if (kcv != null) {
					String msg = String.format("Ks KCV : 0x%02X 0x%02X 0x%02X", kcv[0], kcv[1], kcv[2]);
					alert(msg);
				} 
				break;
			case R.id.getKprivacy:
				byte[] kprivacy = morphoDevice.getKCV(MorphoKCVID.ID_KPRIVACY, null);
				if (kprivacy != null) {
					String msg = String.format("Kprivacy KCV: 0x%02X 0x%02X 0x%02X", kprivacy[0], kprivacy[1], kprivacy[2]);
					alert(msg);
				} 
				break;
			case R.id.enableBioDataEncryption:
				if (ProcessInfo.getInstance().getBioDataEncryptionState() == true) {
					morphoDevice.enableDataEncryption(false, "");
					menuItemEnableBioDataEncryption.setTitle(getResources().getString(R.string.enableBioDataEncryption));
					ProcessInfo.getInstance().setBioDataEncryptionState(false);
				} else {
					// Display popup to enter diversification data
					LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					View layout = inflater.inflate(R.layout.set_diversification_data, (ViewGroup) findViewById(R.id.set_diversification_data));
					final PopupWindow pw = new PopupWindow(layout, ListPopupWindow.WRAP_CONTENT, ListPopupWindow.WRAP_CONTENT, true);
					pw.showAtLocation(layout, Gravity.CENTER, 0, 0);
					Button validationButton = (Button) layout.findViewById(R.id.validation_btn);
					validationButton.setOnClickListener(new View.OnClickListener() {
		                @Override
		                public void onClick(View v) {
		                	EditText diversification_data_edit_txt = (EditText)pw.getContentView().findViewById(R.id.diversification_data_txt);
		                	String diversification_data = diversification_data_edit_txt.getText().toString();
		                	if (!diversification_data.isEmpty()) {
		                		morphoDevice.enableDataEncryption(true, diversification_data);
		                		menuItemEnableBioDataEncryption.setTitle(getResources().getString(R.string.diableBioDataEncryption));
		                		ProcessInfo.getInstance().setBioDataEncryptionState(true);
		                	}
		                	pw.dismiss();
		                }
					});
					Button cancelButton = (Button) layout.findViewById(R.id.cancel_btn);
		            cancelButton.setOnClickListener(new View.OnClickListener() {
		                public void onClick(View v) {
		                    pw.dismiss();
		                }
		            });
				}
				break;
			case R.id.decryptBioData:
				/********************************************************
				 Not working with data crypted in Asymetric secure mode
				*********************************************************/
				alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
				alertDialog.setMessage(getString(R.string.selectBioData));
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						currentAction = MorphoSampleAction.onDecryptBioData;
						try {
							Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
							startActivityForResult(activityIntent, 0);

						} catch (Exception e) {
						}
					}
				});
				alertDialog.show();
				break;
			case R.id.encryptAES128:
				alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
				alertDialog.setMessage(getString(R.string.selectAes128Key));
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						currentAction = MorphoSampleAction.onAesEncrypt;
						try {
							Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
							startActivityForResult(activityIntent, 0);

						} catch (Exception e) {
						}
					}
				});
				alertDialog.show();
				break;
			case R.id.decryptAES128:
				alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
				alertDialog.setMessage(getString(R.string.selectAes128Key));
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						currentAction = MorphoSampleAction.onAesDecrypt;
						try {
							Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
							startActivityForResult(activityIntent, 0);

						} catch (Exception e) {
						}
					}
				});
				alertDialog.show();
				break;
			case R.id.privacyMode:
				LayoutInflater factoryPrivacyMode = LayoutInflater.from(this);
				final View textEntryViewPrivacyMode = factoryPrivacyMode.inflate(R.layout.privacymode, null);
					
				final RadioGroup radioprivacymode = (RadioGroup) textEntryViewPrivacyMode.findViewById(R.id.radioPrivacyMode);
				final RadioGroup radioprivacymodeDB = (RadioGroup) textEntryViewPrivacyMode.findViewById(R.id.radioPrivacyModeDB);
				
				final AlertDialog alertDialogPrivacyMode = new AlertDialog.Builder(this).create();
				alertDialogPrivacyMode.setTitle(this.getResources().getString(R.string.morphosample));
				alertDialogPrivacyMode.setView(textEntryViewPrivacyMode);
				
				alertDialogPrivacyMode.setButton(DialogInterface.BUTTON_NEGATIVE, this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {				
					}
				});
				
				alertDialogPrivacyMode.setButton(DialogInterface.BUTTON_POSITIVE, this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which) {					
						int ret = 0;						
						if(radioprivacymode.getCheckedRadioButtonId() == R.id.radioButtonEnablePrivacy)	{	// Enable Privacy Mode
							if(radioprivacymodeDB.getCheckedRadioButtonId() == R.id.radioButtonPMNoOpDB) {
								ret = morphoDevice.setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_ENABLED,MorphoDevicePrivacyModeDBProcessingChoice.PRIVACY_MODE_DB_PROCESSING_NOTHING);
								if (ret == ErrorCodes.MORPHO_OK) {
									if (ProcessInfo.getInstance().getPrivacyKey() == null) {
										// Privacy key shall be reloaded
										if (null != getPrivacyKey()) {
											ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_ENABLED);
											menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusEnabled));
											initDatabaseInformations();
										}
									}
								} else if (ErrorCodes.MORPHOERR_SECU == ret) {	
									if (ProcessInfo.getInstance().isBaseStatusOk()) {
										byte[] kCV = morphoDevice.getKCV(MorphoKCVID.ID_KPRIVACY, null);
										if (null == kCV) {
											alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeSecuError1));
								} else {
											alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeSecuError2));
										}
									} else {
										alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeSecuError3));
									}
								} else {
									alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeError));
								}
							} else if(radioprivacymodeDB.getCheckedRadioButtonId() == R.id.radioButtonPMEraseDB) {
								ret = morphoDevice.setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_ENABLED,MorphoDevicePrivacyModeDBProcessingChoice.PRIVACY_MODE_DB_PROCESSING_ERASE);
								if (ret == ErrorCodes.MORPHO_OK) {
									if (ProcessInfo.getInstance().getPrivacyKey() == null) {
										// Privacy key shall be reloaded
										if (null != getPrivacyKey()) {
									ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_ENABLED);
									menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusEnabled));
											// force database destroy
											destroyBase();
										}
									}
								} else if (ret == ErrorCodes.MORPHOERR_SECU) {
									alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeSecuError3));
								} else {
									alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeError));
								}
							} else if(radioprivacymodeDB.getCheckedRadioButtonId() == R.id.radioButtonPMCipherDB) {
								ret = morphoDevice.setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_ENABLED,MorphoDevicePrivacyModeDBProcessingChoice.PRIVACY_MODE_DB_PROCESSING_CIPHER_TRANCIPHER);
								if (ret == ErrorCodes.MORPHO_OK) {
									if (ProcessInfo.getInstance().getPrivacyKey() == null) {
										// Privacy key shall be reloaded
										if (null != getPrivacyKey()) {
									ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_ENABLED);
									menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusEnabled));
											initDatabaseInformations();
										}
									}
								} else if (ret == ErrorCodes.MORPHOERR_SECU) {
									alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeSecuError3));
								} else {
									alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeError));
								}
							}
						} else if(radioprivacymode.getCheckedRadioButtonId() == R.id.radioButtonPartialEnablePrivacy) {	 // Enable Partial Privacy Mode
							if(radioprivacymodeDB.getCheckedRadioButtonId() == R.id.radioButtonPMNoOpDB) {
								ret = morphoDevice.setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_PARTIAL_ENABLED,MorphoDevicePrivacyModeDBProcessingChoice.PRIVACY_MODE_DB_PROCESSING_NOTHING);
								if (ret == ErrorCodes.MORPHO_OK) {
									if (ProcessInfo.getInstance().getPrivacyKey() == null) {
										// Privacy key shall be reloaded
										if (null != getPrivacyKey()) {
									ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_PARTIAL_ENABLED);
									menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusPartialEnabled));
											initDatabaseInformations();
										}
									}
								} else if (ErrorCodes.MORPHOERR_SECU == ret) {	
									if (ProcessInfo.getInstance().isBaseStatusOk()) {
										byte[] kCV = morphoDevice.getKCV(MorphoKCVID.ID_KPRIVACY, null);
										if (null == kCV) {
											alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeSecuError1));
								} else {
											alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeSecuError2));
										}
									} else {
										alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeSecuError3));
									}
								} else {
									alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeError));
								}
							} else if(radioprivacymodeDB.getCheckedRadioButtonId() == R.id.radioButtonPMEraseDB) {
								ret = morphoDevice.setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_PARTIAL_ENABLED,MorphoDevicePrivacyModeDBProcessingChoice.PRIVACY_MODE_DB_PROCESSING_ERASE);
								if (ret == ErrorCodes.MORPHO_OK) {
									if (ProcessInfo.getInstance().getPrivacyKey() == null) {
										// Privacy key shall be reloaded
										if (null != getPrivacyKey()) {
									ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_PARTIAL_ENABLED);
									menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusPartialEnabled));
											// force database destroy
											destroyBase();
										}
									}
								} else if (ret == ErrorCodes.MORPHOERR_SECU) {
									alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeSecuError3));
								} else {
									alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeError));
								}
							} else if(radioprivacymodeDB.getCheckedRadioButtonId() == R.id.radioButtonPMCipherDB) {
								ret = morphoDevice.setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_PARTIAL_ENABLED,MorphoDevicePrivacyModeDBProcessingChoice.PRIVACY_MODE_DB_PROCESSING_CIPHER_TRANCIPHER);
								if (ret == ErrorCodes.MORPHO_OK) {
									if (ProcessInfo.getInstance().getPrivacyKey() == null) {
										// Privacy key shall be reloaded
										if (null != getPrivacyKey()) {
									ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_PARTIAL_ENABLED);
									menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusPartialEnabled));
											initDatabaseInformations();
										}
									}
								} else if (ret == ErrorCodes.MORPHOERR_SECU) {
									alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeSecuError3));
								} else {
									alert(ret, 0, getResources().getString(R.string.privacyMode), getResources().getString(R.string.enablePrivacyModeError));
								}
							}
						} else if(radioprivacymode.getCheckedRadioButtonId() == R.id.radioButtonDisablePrivacy) {	// Disable Privacy Mode
							if(radioprivacymodeDB.getCheckedRadioButtonId() == R.id.radioButtonPMNoOpDB) {
								ret = morphoDevice.setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED,MorphoDevicePrivacyModeDBProcessingChoice.PRIVACY_MODE_DB_PROCESSING_NOTHING);
								if (ret == ErrorCodes.MORPHO_OK) {
									ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED);
									menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusDisabled));
									ProcessInfo.getInstance().setPrivacyKey(null);
									initDatabaseInformations();
								} else if (ErrorCodes.MORPHOERR_SECU == ret) {	
									alert(ret, 0, getResources().getString(R.string.disablePrivacyMode), getResources().getString(R.string.disablePrivacyModeSecuError));
								} else {
									alert(ret, 0, getResources().getString(R.string.disablePrivacyMode), getResources().getString(R.string.disablePrivacyModeError));
								}							
							} else if(radioprivacymodeDB.getCheckedRadioButtonId() == R.id.radioButtonPMEraseDB) {
								ret = morphoDevice.setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED,MorphoDevicePrivacyModeDBProcessingChoice.PRIVACY_MODE_DB_PROCESSING_ERASE);
								if (ret == ErrorCodes.MORPHO_OK) {
									ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED);
									menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusDisabled));
									ProcessInfo.getInstance().setPrivacyKey(null);
									// force database destroy
									destroyBase();
								} else {
									alert(ret, 0, getResources().getString(R.string.disablePrivacyMode), getResources().getString(R.string.disablePrivacyModeError));
								}
							} else if(radioprivacymodeDB.getCheckedRadioButtonId() == R.id.radioButtonPMCipherDB) {
								ret = morphoDevice.setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED,MorphoDevicePrivacyModeDBProcessingChoice.PRIVACY_MODE_DB_PROCESSING_CIPHER_TRANCIPHER);
								if (ret == ErrorCodes.MORPHO_OK) {
									ProcessInfo.getInstance().setPrivacyModeStatus(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED);
									menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusDisabled));
									ProcessInfo.getInstance().setPrivacyKey(null);
									initDatabaseInformations();
								} else {
									alert(ret, 0, getResources().getString(R.string.disablePrivacyMode), getResources().getString(R.string.disablePrivacyModeError));
								}
							}
						}						
						// Set event
						handler.sendMessage(handler.obtainMessage());
					}
				});
				alertDialogPrivacyMode.show();
				
				// Wait for event click
				try {
					Looper.loop();
				} catch (RuntimeException e) {
					
				}
				
				break;

			case R.id.userAreaData:
				LayoutInflater factoryuserAreaData = LayoutInflater.from(this);
				final View textEntryViewuserAreaData = factoryuserAreaData.inflate(R.layout.userareadata, null);

				final RadioGroup radiouserAreaData = (RadioGroup) textEntryViewuserAreaData.findViewById(R.id.radioUserAreaData);

				final AlertDialog alertDialogUserAreaData = new AlertDialog.Builder(this).create();
				alertDialogUserAreaData.setTitle(this.getResources().getString(R.string.morphosample));
				alertDialogUserAreaData.setView(textEntryViewuserAreaData);

				alertDialogUserAreaData.setButton(DialogInterface.BUTTON_NEGATIVE, this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});

				alertDialogUserAreaData.setButton(DialogInterface.BUTTON_POSITIVE, this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
				{
				public void onClick(DialogInterface dialog, int which) {
					int ret = 0;
					if(radiouserAreaData.getCheckedRadioButtonId() == R.id.radioReadUserAreaData) {    // Read User Area Data
						byte[] UserDataByte = morphoDevice.getUserAreaData();
						if ( UserDataByte != null  ) {
							String output_file = "";
							try {
								output_file = "UserAreaData";
								FileOutputStream fos = new FileOutputStream(AppContext.RootPath + output_file + ".bin");
								fos.write(UserDataByte);
								fos.close();
								String message = String.format(getResources().getString(R.string.readUserAreaData), AppContext.RootPath + output_file + ".bin");
								alert(message);
							} catch (IOException e) {
								Log.e("tag", "Failed to copy User Area Data file: " + output_file, e);
							}
					}
					else {
							String mes = getResources().getString(R.string.UserAreaDataEmpty);
							alert(mes);
					}
					}
					else if(radiouserAreaData.getCheckedRadioButtonId() == R.id.radioWriteUserAreaData){ // Write User Area Data

						alertDialog.setTitle(getResources().getString(R.string.morphosample));
						alertDialog.setMessage(getString(R.string.selectUserAreaData));
						alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								currentAction = MorphoSampleAction.onWriteUserAreaData;
								try {
									Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
									startActivityForResult(activityIntent, 0);
								} catch (Exception e) {
								}
							}
						});
						alertDialog.show();

					}
					else if(radiouserAreaData.getCheckedRadioButtonId() == R.id.radioRemoveUserAreaData){ // Remove User Area Data

						ret = morphoDevice.removeUserAreaData();
						if (ret == ErrorCodes.MORPHO_OK) {
							String mes = getResources().getString(R.string.UserAreaDataremove);
							alert(mes);
						}
						else {
							String mes = getResources().getString(R.string.UserAreaDataremoveError);
							alert(mes);
						}
					}

		}
				});
				alertDialogUserAreaData.show();

				// Wait for event click
				try {
					Looper.loop();
				} catch (RuntimeException e) {

				}

				break;

			case R.id.loadKprivacyUnsecureMode:
				alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
				alertDialog.setMessage(getString(R.string.selectExternalKey));
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						currentAction = MorphoSampleAction.onLoadKprivacyUnsecureMode;
						try {
							Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
							startActivityForResult(activityIntent, 0);
						} catch (Exception e) {
						}
					}
				});
				alertDialog.show();
				break;
			case R.id.loadKprivacySymmetricSecureMode:
				alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
				alertDialog.setMessage(getString(R.string.selectCurrentKey));
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						currentAction = MorphoSampleAction.onLoadKprivacySymmetricSecureMode;
						try {
							Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
							startActivityForResult(activityIntent, 0);

						} catch (Exception e) {
						}
					}
				});
				alertDialog.show();
				
				break;
			default:
				break;
		}
		
		return super.onOptionsItemSelected(item);
	}

	private void copyLogFileParam()
	{
		String filename = "Log.ini";
		try {			
			File logFileParam = new File(AppContext.RootPath, filename);
			if (!logFileParam.exists()) {
				AssetManager assetManager = getAssets();
				InputStream in = null;
				OutputStream out = null;

				in = assetManager.open(filename);
				out = new FileOutputStream(AppContext.RootPath + filename);
				copyFile(in, out);
				in.close();
				in = null;
				out.flush();
				out.close();
				out = null;
			}
		} catch (IOException e) {
			Log.e("tag", "Failed to copy asset file: " + filename, e);
		}
	}
	
	private void copyFile(InputStream in, OutputStream out) throws IOException
	{
	      byte[] buffer = new byte[1024];
	      int read;
	      while((read = in.read(buffer)) != -1)
	      {
	            out.write(buffer, 0, read);
	      }
	}
	
	protected void alert(int codeError, int internalError, String title, String message)
	{
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(title);
		String msg;
		if (codeError == 0)
		{
			msg = getResources().getString(R.string.OP_SUCCESS);
		}
		else
		{
			msg = getResources().getString(R.string.OP_FAILED)+"\n" + ErrorCodes.getError(codeError, internalError);
		}
		msg += ((message.equalsIgnoreCase("")) ? "" : "\n" + message);
		alertDialog.setMessage(msg);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
			}
		});
		alertDialog.show();
	}

	protected void alertClose(String title, String message)
	{
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				finish();
			}
		});
		alertDialog.show();
	}
	
	private void initDatabaseInformations() {
		// setting maximum number of templates allowed per user 
		Integer nbFinger = new Integer(0);
		morphoDatabase.getNbFinger(nbFinger);
		ProcessInfo.getInstance().setNumberOfFingerPerRecord(nbFinger);

		//setting number of used records in database
		Long nbUsedRecord = new Long(0);
		morphoDatabase.getNbUsedRecord(nbUsedRecord);
		ProcessInfo.getInstance().setCurrentNumberOfUsedRecordValue(nbUsedRecord);

		Long nbTotalRecord = new Long(0);
		morphoDatabase.getNbTotalRecord(nbTotalRecord);
		ProcessInfo.getInstance().setMaximumNumberOfRecordValue(nbTotalRecord);
		
		Integer status = new Integer(0);
		int ret = morphoDatabase.getDbEncryptionStatus(status);
		
		if(ret != ErrorCodes.MORPHO_OK) {
			ProcessInfo.getInstance().setEncryptDatabaseValue("N/A");
		} else {
			String databaseStatus = "NO";
			if (status==1) {
				databaseStatus = "YES";
				if (MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {
					databaseStatus += " (Privacy Mode)";
		}
		}
			ProcessInfo.getInstance().setEncryptDatabaseValue(databaseStatus);
		}

		try	{
			TextView maxnb = (TextView) findViewById(R.id.maximumnumberofrecordvalue);
			maxnb.setText(Long.toString(ProcessInfo.getInstance().getMaximumNumberOfRecordValue()));
		} catch (Exception e) {
		}

		try	{
			TextView curnb = (TextView) findViewById(R.id.currentnumberofusedrecordvalue);
			curnb.setText(Long.toString(ProcessInfo.getInstance().getCurrentNumberOfUsedRecordValue()));
		} catch (Exception e) {
		}

		try {
			TextView nbFin = (TextView) findViewById(R.id.numberoffingerperrecordvalue);
			nbFin.setText(Integer.toString(ProcessInfo.getInstance().getNumberOfFingerPerRecord()));
		} catch (Exception e) {
		}
		
		TextView encryptDatabaseStatus = (TextView) findViewById(R.id.encryptDatabase);
		encryptDatabaseStatus.setText(ProcessInfo.getInstance().getEncryptDatabaseValue());
	}

	private void initOptionsInformations()
	{
		boolean iv = ProcessInfo.getInstance().isImageViewer();
		((CheckBox) findViewById(R.id.imageviewer)).setChecked(iv);
		boolean apc = ProcessInfo.getInstance().isAsyncPositioningCommand();
		((CheckBox) findViewById(R.id.asyncpositioningcommand)).setChecked(apc);
		boolean aec = ProcessInfo.getInstance().isAsyncEnrollmentCommand();
		((CheckBox) findViewById(R.id.asyncenrollmentcommand)).setChecked(aec);
		boolean adq = ProcessInfo.getInstance().isAsyncDetectQuality();
		((CheckBox) findViewById(R.id.asyncdetectquality)).setChecked(adq);
		boolean acq = ProcessInfo.getInstance().isAsyncCodeQuality();
		((CheckBox) findViewById(R.id.asynccodequality)).setChecked(acq);

		boolean emn = ProcessInfo.getInstance().isExportMatchingPkNumber();
		((CheckBox) findViewById(R.id.exportmatchingpknumber)).setChecked(emn);
		boolean wlo = ProcessInfo.getInstance().isWakeUpWithLedOff();
		((CheckBox) findViewById(R.id.wakeupwithledoff)).setChecked(wlo);

		// Set spinner content from enum value for SensorWindowPosition
		// -----------------------------------------------------------------------------------------
		Spinner spinner = (Spinner) findViewById(R.id.sensorwindowposition);
		CharSequence[] itemArray = new CharSequence[SensorWindowPosition.values().length];
		int i = 0;
		for (SensorWindowPosition value : SensorWindowPosition.values())
		{
			itemArray[i++] = value.getLabel();
		}
		List<CharSequence> itemList = new ArrayList<CharSequence>(Arrays.asList(itemArray));
		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item, itemList);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);

		byte[] sensorWindowPosition = morphoDevice.getConfigParam(MorphoDevice.CONFIG_SENSOR_WIN_POSITION_TAG);
		int position = 0;
		if (sensorWindowPosition != null)
		{
			position = sensorWindowPosition[0];
			if (position > 3)
				position = 0;
		}
		ProcessInfo.getInstance().setSensorWindowPosition(SensorWindowPosition.values()[position]);

		int spinnerPosition = adapter.getPosition(ProcessInfo.getInstance().getSensorWindowPosition().getLabel());
		spinner.setSelection(spinnerPosition);
		// listen to the event
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				if (defaultWindowSensorPositionValue == true)
				{
					defaultWindowSensorPositionValue = false;
				}
				else
				{
					SensorWindowPosition item = SensorWindowPosition.fromString((String) parent.getItemAtPosition(pos));
					ProcessInfo.getInstance().setSensorWindowPosition(item);
					byte[] paramValue = new byte[1];
					paramValue[0] = (byte) item.getCode();
					int ret = morphoDevice.setConfigParam(MorphoDevice.CONFIG_SENSOR_WIN_POSITION_TAG, paramValue);
					if (ret != ErrorCodes.MORPHO_OK)
					{
						alert(ret, 0, "MorphoDevice.setConfigParam", "");
					}
					else
					{
						alert(ret,0,"Sensor Window Position","You must restart the sensor through \"Reboot soft\" function before using this parameter!");					
					}
				}
			}

			public void onNothingSelected(AdapterView<?> parent)
			{
			}
		});

		// Set CONFIG KEY USER content from enum value for SensorWindowPosition
		// -----------------------------------------------------------------------------------------
		byte[] ret = morphoDevice.getConfigParam(MorphoDevice.CONFIG_KEY_USER_TAG);
		CheckBox keyUserConfig = (CheckBox) findViewById(R.id.configkeyuser);

		if(ret != null) {
			if (ret[0] == 0) {
				keyUserConfig.setChecked(false);
			} else if (ret[0] == 1) {
				keyUserConfig.setChecked(true);
			} else {
				keyUserConfig.setEnabled(false);
			}
		} else {
			keyUserConfig.setEnabled(false);
		}


	}

	private void initBioSettingsInformations()
	{
		// Init MatchingThreshold
		// -----------------------------------------------------------------------------------------
		final EditText mt = (EditText) findViewById(R.id.matchingthresholdvalue);
		mt.setText(Integer.toString(ProcessInfo.getInstance().getMatchingThreshold()));
		mt.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void afterTextChanged(Editable arg0)
			{
				try
				{
					final int set = ProcessInfo.getInstance().setMatchingThreshold(Integer.parseInt(arg0.toString().trim()));
					if (set != Integer.parseInt(arg0.toString().trim()))
					{
						mHandler.post(new Runnable()
						{
							@Override
							public synchronized void run()
							{
								mt.setText(Integer.toString(set));
							}
						});
					}
				}
				catch (Exception e)
				{
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
			}
		});

		// Init Timeout
		// -----------------------------------------------------------------------------------------
		EditText to = (EditText) findViewById(R.id.timeoutsecvalue);
		to.setText(Integer.toString(ProcessInfo.getInstance().getTimeout()));
		to.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void afterTextChanged(Editable arg0)
			{
				try
				{
					ProcessInfo.getInstance().setTimeout(Integer.parseInt(arg0.toString().trim()));
				}
				catch (Exception e)
				{
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
			}
		});

		// Set spinner content from enum value for CoderChoice
		// -----------------------------------------------------------------------------------------
		Spinner spinnerCC = (Spinner) findViewById(R.id.coderchoice);
		CharSequence[] itemCCArray = new CharSequence[Coder.values().length];
		int iCC = 0;
		for (Coder value : Coder.values())
		{
			itemCCArray[iCC++] = value.getLabel();
		}
		List<CharSequence> itemCCList = new ArrayList<CharSequence>(Arrays.asList(itemCCArray));
		ArrayAdapter<CharSequence> adapterCC = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item, itemCCList);
		adapterCC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerCC.setAdapter(adapterCC);
		// set the default according to value
		int spinnerCCPosition = adapterCC.getPosition(ProcessInfo.getInstance().getCoder().getLabel());
		spinnerCC.setSelection(spinnerCCPosition);
		// listen to the event
		spinnerCC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				if (defaultCoderChoiceValue == true)
				{
					defaultCoderChoiceValue = false;
				}
				else
				{
					String item = (String) parent.getItemAtPosition(pos);
					ProcessInfo.getInstance().setCoder(Coder.fromString(item));
				}
			}

			public void onNothingSelected(AdapterView<?> parent)
			{
			}
		});

		// Set spinner content from enum value for SecurityLevel
		// -----------------------------------------------------------------------------------------
		Spinner spinnerSL = (Spinner) findViewById(R.id.securitylevel);
		CharSequence[] itemSLArray = null;

		if (MorphoInfo.m_b_fvp) //FVP
		{
			itemSLArray = new CharSequence[3];

			itemSLArray[0] = SecurityLevel.MULTIMODAL_SECURITY_STANDARD.getLabel();
			itemSLArray[1] = SecurityLevel.MULTIMODAL_SECURITY_MEDIUM.getLabel();
			itemSLArray[2] = SecurityLevel.MULTIMODAL_SECURITY_HIGH.getLabel();
		}
		else
		// MSO
		{
			itemSLArray = new CharSequence[5];

			itemSLArray[0] = SecurityLevel.FFD_SECURITY_LEVEL_DEFAULT_HOST.getLabel();
			itemSLArray[1] = SecurityLevel.FFD_SECURITY_LEVEL_LOW_HOST.getLabel();
			itemSLArray[2] = SecurityLevel.FFD_SECURITY_LEVEL_MEDIUM_HOST.getLabel();
			itemSLArray[3] = SecurityLevel.FFD_SECURITY_LEVEL_HIGH_HOST.getLabel();
			itemSLArray[4] = SecurityLevel.FFD_SECURITY_LEVEL_CRITICAL_HOST.getLabel();

		}

		List<CharSequence> itemSLList = new ArrayList<CharSequence>(Arrays.asList(itemSLArray));
		ArrayAdapter<CharSequence> adapterSL = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item, itemSLList);
		adapterSL.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerSL.setAdapter(adapterSL);

		int securityLevelValue = morphoDevice.getSecurityLevel();

		SecurityLevel securityLevel = SecurityLevel.fromInt(securityLevelValue, MorphoInfo.m_b_fvp);

		int spinnerSLPosition = adapterSL.getPosition(securityLevel.getLabel());
		spinnerSL.setSelection(spinnerSLPosition);
		ProcessInfo.getInstance().setSecurityLevel(securityLevel);
		// listen to the event
		spinnerSL.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				if (defaultSecurityLevelValue == true)
				{
					defaultSecurityLevelValue = false;
				}
				else
				{
					String label = (String) parent.getItemAtPosition(pos);
					SecurityLevel securityLevel = SecurityLevel.fromString(label, MorphoInfo.m_b_fvp);

					int l_ret = morphoDevice.setSecurityLevel(securityLevel);

					if (l_ret != ErrorCodes.MORPHO_OK)
					{
						alert(l_ret, 0, "setSecurityLevel", "");
					}

					ProcessInfo.getInstance().setSecurityLevel(securityLevel);
				}
			}

			public void onNothingSelected(AdapterView<?> parent)
			{
			}
		});

		// Set spinner content from enum value for MatchingStrategy
		// -----------------------------------------------------------------------------------------
		Spinner spinnerMS = (Spinner) findViewById(R.id.matchingstrategy);
		CharSequence[] itemMSArray = new CharSequence[MatchingStrategy.values().length];
		int i = 0;
		for (MatchingStrategy value : MatchingStrategy.values())
		{
			itemMSArray[i++] = value.getLabel();
		}
		List<CharSequence> itemMSList = new ArrayList<CharSequence>(Arrays.asList(itemMSArray));
		ArrayAdapter<CharSequence> adapterMS = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item, itemMSList);
		adapterMS.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerMS.setAdapter(adapterMS);
		// set the default according to value
		int spinnerMSPosition = adapterMS.getPosition(ProcessInfo.getInstance().getMatchingStrategy().getLabel());
		spinnerMS.setSelection(spinnerMSPosition);
		// listen to the event
		spinnerMS.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				if (defaultMatchingStrategyValue == true)
				{
					defaultMatchingStrategyValue = false;
				}
				else
				{
					String item = (String) parent.getItemAtPosition(pos);
					ProcessInfo.getInstance().setMatchingStrategy(MatchingStrategy.fromString(item));
				}
			}

			public void onNothingSelected(AdapterView<?> parent)
			{
			}
		});
		
		// Set spinner content from enum value for Strategy Acquisition Mode
		// -----------------------------------------------------------------------------------------
		Spinner spinnerAS = (Spinner) findViewById(R.id.acquisitionStrategy);
		CharSequence[] itemASArray = new CharSequence[StrategyAcquisitionMode.values().length];
		i = 0;
		for (StrategyAcquisitionMode value : StrategyAcquisitionMode.values())
		{
			itemASArray[i++] = value.getLabel();
		}
		List<CharSequence> itemASList = new ArrayList<CharSequence>(Arrays.asList(itemASArray));
		ArrayAdapter<CharSequence> adapterAS = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_spinner_item, itemASList);
		adapterAS.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerAS.setAdapter(adapterAS);
		// set the default according to value
		int spinnerASPosition = adapterAS.getPosition(ProcessInfo.getInstance().getStrategyAcquisitionMode().getLabel());
		spinnerAS.setSelection(spinnerASPosition);
		// listen to the event
		spinnerAS.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				if (defaultStrategyAcquisitionModeValue == true)
				{
					defaultStrategyAcquisitionModeValue = false;
				}
				else
				{
					String item = (String) parent.getItemAtPosition(pos);
					ProcessInfo.getInstance().setStrategyAcquisitionMode(StrategyAcquisitionMode.fromString(item));
				}
			}

			public void onNothingSelected(AdapterView<?> parent)
			{
			}
		});
		//-----------------------------------------------------------------------------------------
		CheckBox ffot = (CheckBox) findViewById(R.id.forcefingerplacementontop);
		ffot.setChecked(ProcessInfo.getInstance().isForceFingerPlacementOnTop());
		
		CheckBox adSecComReq = (CheckBox) findViewById(R.id.advancedseclevelcompatibilityreq);
		if(!MorphoInfo.m_b_fvp)
		{
			adSecComReq.setEnabled(false);
		}

		CheckBox fqt = (CheckBox) findViewById(R.id.fingerqualitythreshold);
		fqt.setChecked(ProcessInfo.getInstance().isFingerprintQualityThreshold());

		// Init Finger Quality Threshold Value
		// -----------------------------------------------------------------------------------------
		EditText fqv = (EditText) findViewById(R.id.fingerqualitythresholdvalue);
		fqv.setText(Integer.toString(ProcessInfo.getInstance().getFingerprintQualityThresholdvalue()));
		fqv.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void afterTextChanged(Editable arg0)
			{
				try
				{
					ProcessInfo.getInstance().setFingerprintQualityThresholdvalue(Integer.parseInt(arg0.toString().trim()));
				}
				catch (Exception e)
				{
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
			}
		});
		fqv.setEnabled(ProcessInfo.getInstance().isFingerprintQualityThreshold());		
	}

	private void initTabHost()
	{
		// Initialize the tabs :
		// --------------------------------
		final TabHost tabHost = getTabHost();

		// Tab for Capture
		TabSpec capturespec = tabHost.newTabSpec(captureTag);
		// setting Title and Icon for the Tab
		capturespec.setIndicator(getResources().getString(R.string.title_activity_capture), getResources().getDrawable(R.drawable.ic_launcher));
		Intent captureIntent = new Intent(this, CaptureActivity.class);
		capturespec.setContent(captureIntent);

		// Tab for Verify
		TabSpec verifyspec = tabHost.newTabSpec(verifyTag);
		verifyspec.setIndicator(getResources().getString(R.string.title_activity_verify), getResources().getDrawable(R.drawable.ic_launcher));
		Intent verifyIntent = new Intent(this, VerifyActivity.class);
		verifyspec.setContent(verifyIntent);

		// Tab for GetFingerPrint Image
		TabSpec fingerPrintSpec = tabHost.newTabSpec(fingerPrintTag);
		fingerPrintSpec.setIndicator(getResources().getString(R.string.title_activity_fingerPrint), getResources().getDrawable(R.drawable.ic_launcher));
		Intent fingerPrintIntent = new Intent(this, FingerPrintActivity.class);
		fingerPrintSpec.setContent(fingerPrintIntent);

		// Tab for Enroll
		TabSpec enrollspec = tabHost.newTabSpec(enrollTag);
		enrollspec.setIndicator(getResources().getString(R.string.title_activity_enroll), getResources().getDrawable(R.drawable.ic_launcher));
		Intent enrollIntent = new Intent(this, EnrollActivity.class);
		enrollspec.setContent(enrollIntent);

		// Tab for Identify
		TabSpec identifyspec = tabHost.newTabSpec(identifyTag);
		identifyspec.setIndicator(getResources().getString(R.string.title_activity_identify), getResources().getDrawable(R.drawable.ic_launcher));
		Intent identifyIntent = new Intent(this, IdentifyActivity.class);
		identifyspec.setContent(identifyIntent);

		// Tab for Process
		TabSpec processspec = tabHost.newTabSpec(processTag);
		processspec.setIndicator(getResources().getString(R.string.title_activity_process), getResources().getDrawable(R.drawable.ic_launcher));
		Intent processIntent = new Intent(this, ProcessActivity.class);
		processspec.setContent(processIntent);

		// Adding all TabSpec to TabHost
		try
		{
			tabHost.addTab(capturespec); // Adding Capture tab
		}
		catch (Exception e1)
		{
			e1.getMessage();
		}
		try
		{
			tabHost.addTab(verifyspec); // Adding Verify tab
		}
		catch (Exception e2)
		{
			e2.getMessage();
		}
		try
		{
			tabHost.addTab(fingerPrintSpec); // Adding FingerPrint Image tab
		}
		catch (Exception e3)
		{
			e3.getMessage();
		}
		try
		{
			tabHost.addTab(enrollspec); // Adding Enroll tab
		}
		catch (Exception e4)
		{
			e4.getMessage();
		}
		try
		{
			tabHost.addTab(identifyspec); // Adding Identify tab
		}
		catch (Exception e5)
		{
			e5.getMessage();
		}

		try
		{
			tabHost.addTab(processspec); // Adding Process tab
			tabHost.getTabWidget().getChildTabViewAt(PROCESS_TAB_INDEX).setEnabled(false);
			tabHost.getTabWidget().getChildTabViewAt(PROCESS_TAB_INDEX).setBackgroundColor(Color.LTGRAY);
			tabHost.getTabWidget().getChildTabViewAt(PROCESS_TAB_INDEX).setVisibility(View.GONE);

		}
		catch (Exception e6)
		{
			e6.getMessage();
		}

		tabHost.setOnTabChangedListener(new OnTabChangeListener()
		{
			// invalidate Process tab if any other tab is clicked !
			public void onTabChanged(String tabId)
			{
				switch (tabHost.getCurrentTab())
				{
					case 0:
					case 1:
					case 2:
					case 3:
						tabHost.getTabWidget().getChildTabViewAt(PROCESS_TAB_INDEX).setBackgroundColor(Color.LTGRAY);
						break;
					default:
						break;
				}
			}
		});
	}

	static List<DatabaseItem>	databaseItems	= null;

	public int loadDatabaseItem()
	{
		int ret = 0;
		databaseItems = new ArrayList<DatabaseItem>();
		int[] indexDescriptor = new int[3];
		indexDescriptor[0] = 0;
		indexDescriptor[1] = 1;
		indexDescriptor[2] = 2;

		MorphoUserList morphoUserList = new MorphoUserList();
		ret = morphoDatabase.readPublicFields(indexDescriptor, morphoUserList);
		
		int l_nb_user = morphoUserList.getNbUser();
		for (int i = 0; i < l_nb_user; i++)
		{
			MorphoUser morphoUser = morphoUserList.getUser(i);
			
			if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) { // Privacy Mode enabled
				byte[] 	userID = morphoUser.getBufferField(0);
				byte[]  firstName = morphoUser.getBufferField(1);
				byte[]  lastName = morphoUser.getBufferField(2);
				
				byte[] privacyKey = ProcessInfo.getInstance().getPrivacyKey();
				if (null != privacyKey) {
					byte[] idDecrypted = null;
					byte[] firstNameDecrypted = null;
					byte[] lastNameDecrypted = null;
					
					byte[] decrypted_bio_data = decryptAndCheckPrivacyData(userID, privacyKey);
					if (null != decrypted_bio_data)
						idDecrypted = Arrays.copyOfRange(decrypted_bio_data, 8, decrypted_bio_data.length);					
					decrypted_bio_data = decryptAndCheckPrivacyData(firstName, privacyKey);
					if (null != decrypted_bio_data)
						firstNameDecrypted = Arrays.copyOfRange(decrypted_bio_data, 8, decrypted_bio_data.length);
					decrypted_bio_data = decryptAndCheckPrivacyData(lastName, privacyKey);
					if (null != decrypted_bio_data)
						lastNameDecrypted = Arrays.copyOfRange(decrypted_bio_data, 8, decrypted_bio_data.length);
					
					if (null != idDecrypted && null != firstNameDecrypted && null != lastNameDecrypted)
						databaseItems.add(new DatabaseItem(new String(idDecrypted), new String(firstNameDecrypted), new String(lastNameDecrypted)));
				} else {
					alert("Privacy key not found, Data Base will not be decrypted/refreshed");
				}
			} else {
				String userID = morphoUser.getField(0);
				String firstName = morphoUser.getField(1);
				String lastName = morphoUser.getField(2);
				databaseItems.add(new DatabaseItem(userID, firstName, lastName));
			}
		}
		
		ProcessInfo.getInstance().setDatabaseItems(databaseItems);
		initDatabaseItem();
		ProcessInfo.getInstance().setCurrentNumberOfRecordValue(databaseItems.size());
		
		return ret;
	}

	private void initDatabaseItem()
	{

		final List<DatabaseItem> databaseItems = ProcessInfo.getInstance().getDatabaseItems();

		// Initialize the database list
		// --------------------------------
		databaseListView = (ListView) findViewById(R.id.databaselist);
		
		DatabaseArrayAdapter databaseArrayAdapter = new DatabaseArrayAdapter(this, R.layout.database_view, databaseItems);
		databaseListView.setAdapter(databaseArrayAdapter);		

		databaseListView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long id)
			{
				// Take action here.
				DatabaseItem selected = (DatabaseItem) databaseListView.getItemAtPosition(position);
				if (databaseItems != null && selected != null)
				{
					for (int i = 0; i < databaseItems.size(); i++)
					{
						databaseItems.get(i).setSelected(false);
						if (selected.compareTo(databaseItems.get(i)) == 0)
						{
							ProcessInfo.getInstance().setDatabaseSelectedIndex(i);
							databaseItems.get(i).setSelected(true);
						}
					}
					
					for (int i = 0; i < databaseListView.getChildCount(); i++)
					{
						View v = databaseListView.getChildAt(i);
						if (v != null)
						{
							v.setBackgroundColor(Color.TRANSPARENT);
						}
					}
					view.setBackgroundColor(Color.CYAN);
				}				
			}
		});
		ProcessInfo.getInstance().setCurrentNumberOfRecordValue(databaseItems.size());

		if (databaseItems.size() == 0)
		{
			activateButton(true,false);
		}
		else
		{
			activateButton(true,true);
		}
	}

	public void onNoCheckClick(View view)
	{
		CheckBox noCheck = (CheckBox) view;
		ProcessInfo.getInstance().setNoCheck(noCheck.isChecked());
	}

	private void initNoCheck()
	{
		CheckBox savePKinDatabase = (CheckBox) findViewById(R.id.nocheck);
		savePKinDatabase.setChecked(ProcessInfo.getInstance().isNoCheck());
	}

	private void initDatabaseStatus()
	{
		ImageView iv = (ImageView) findViewById(R.id.basestatusimg);
		int databaseStatus = R.drawable.lederror;
		if (ProcessInfo.getInstance().isBaseStatusOk())
		{
			databaseStatus = R.drawable.ledok;
		}
		iv.setBackgroundResource(databaseStatus);
	}

	/**
	 * Alert.
	 * 
	 * @param msg
	 *            the msg
	 * @since 1.0
	 */
	private void alert(String msg)
	{
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
		alertDialog.setMessage(msg);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
			}
		});
		alertDialog.show();
	}
	
	public final void onRebootSoft(View view)
	{				
		enableDisableBoutton(false);

		isRebootSoft = true;
		int ret = morphoDevice.rebootSoft(30,this);
		
		if (ret != 0)
		{	
			alert(getString(R.string.msg_rebootfailure));
		}
	}

	public final void onCloseAndQuit(View view)
	{
		finish();
	}

	public void stopProcess()
	{
		if (ProcessInfo.getInstance().isStarted())
		{
			this.stop(true);
		}
		enableDisableIHM(true);
	}

	private void stop(boolean cancel)
	{
		Button startbutton = (Button) findViewById(R.id.startstop);
		unlockScreenOrientation();
		getTabHost().getTabWidget().getChildTabViewAt(0).setEnabled(true);
		getTabHost().getTabWidget().getChildTabViewAt(1).setEnabled(true);
		getTabHost().getTabWidget().getChildTabViewAt(2).setEnabled(true);
		getTabHost().getTabWidget().getChildTabViewAt(3).setEnabled(true);
		getTabHost().getTabWidget().getChildTabViewAt(4).setEnabled(true);
		getTabHost().getTabWidget().getChildTabViewAt(5).setEnabled(true);

		startbutton.setText(getResources().getString(R.string.startstop));
		ProcessInfo.getInstance().setStarted(false);
		if (ProcessInfo.getInstance().isCommandBioStart())
		{
			if(cancel)
				ProcessInfo.getInstance().getMorphoDevice().cancelLiveAcquisition();
		}
		String currProcessTag = captureTag;
		if (ProcessInfo.getInstance().getMorphoInfo().getClass() == VerifyInfo.class)
		{
			currProcessTag = verifyTag;
		}
		else if (ProcessInfo.getInstance().getMorphoInfo().getClass() == IdentifyInfo.class)
		{
			currProcessTag = identifyTag;
		}
		else if (ProcessInfo.getInstance().getMorphoInfo().getClass() == EnrollInfo.class)
		{
			currProcessTag = enrollTag;
		}
		else if (ProcessInfo.getInstance().getMorphoInfo().getClass() == FingerPrintInfo.class)
		{
			currProcessTag = fingerPrintTag;
		}
		switchTab(currProcessTag);
		if (cancel && currProcessTag == enrollTag && EnrollInfo.getInstance().isSavePKinDatabase())
		{
			refreshNbrOfUsedRecord();
			loadDatabaseItem();
		}
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {

	    menuItemMSOConfiguration = menu.findItem(R.id.msoconfiguration);
	    menuItemLoggingParameters = menu.findItem(R.id.sdkLoggingParameters);
	    menuItemuserAreaData = menu.findItem(R.id.userAreaData);
	    menuItemLoadKsUnsecureMode = menu.findItem(R.id.loadKsUnsecureMode);
	    menuItemLoadKsAsymetricSecureMode = menu.findItem(R.id.loadKsAsymetricSecureMode);
	    menuItemLoadKsSymetricSecureMode = menu.findItem(R.id.loadKsSymetricSecureMode);
	    menuItemGetKs = menu.findItem(R.id.getKs);
	    menuItemGetprivacyKcv = menu.findItem(R.id.getKprivacy);
	    menuItemEnableBioDataEncryption = menu.findItem(R.id.enableBioDataEncryption);
	    menuItemDecryptBioData = menu.findItem(R.id.decryptBioData);
	    
	    menuItemEncryptAESBioData = menu.findItem(R.id.encryptAES128);
	    menuItemDecryptAESBioData = menu.findItem(R.id.decryptAES128);
	    
	    menuItemPrivacyMode = menu.findItem(R.id.privacyMode);
	    menuItemLoadKprivacyUnsecureMode = menu.findItem(R.id.loadKprivacyUnsecureMode);
	    menuItemLoadKprivacySymmetricSecureMode = menu.findItem(R.id.loadKprivacySymmetricSecureMode);
	    
	    if (isOfferedSecurityMode || isTunnelingMode) {
	    	menuItemLoadKsUnsecureMode.setEnabled(false);
	    }
	    if (!isTunnelingMode) {
	    	menuItemLoadKsAsymetricSecureMode.setEnabled(false);
	    }
	    
		MorphoDevicePrivacyModeStatus status = ProcessInfo.getInstance().getPrivacyModeStatus();
		
		if (ErrorCodes.MORPHO_OK != privacy_ret) {
			menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusNotSupported));
			menuItemPrivacyMode.setEnabled(false);
		} else if (status == MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED) {
			menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusDisabled));
		} else {
			// Privacy Mode enabled : get Privacy key
			if (getPrivacyKey() != null) {
				if (status == MorphoDevicePrivacyModeStatus.PRIVACY_MODE_ENABLED) {
					menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusEnabled));
				} else if (status == MorphoDevicePrivacyModeStatus.PRIVACY_MODE_PARTIAL_ENABLED) {
					menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusPartialEnabled));
				} else if (status == MorphoDevicePrivacyModeStatus.PRIVACY_MODE_STANDALONE_ENABLED) {
					menuItemPrivacyMode.setTitle(getResources().getString(R.string.PrivacyMode) + " " + getResources().getString(R.string.PrivacyModeStatusStandalone));
					menuItemPrivacyMode.setEnabled(false);
				} 
			}
		}
		
	    
		return true;
	}

	private void enableDisableIHM(boolean enabled) {
		Button btn = (Button) findViewById(R.id.btn_identitymatch);
		btn.setEnabled(enabled);

		btn = (Button) findViewById(R.id.btn_removeall);
		btn.setEnabled(enabled);

		btn = (Button) findViewById(R.id.btn_removeuser);
		btn.setEnabled(enabled);

		btn = (Button) findViewById(R.id.btn_updateuser);
		btn.setEnabled(enabled);

		btn = (Button) findViewById(R.id.btn_closeandquit);
		btn.setEnabled(enabled);
		
		btn = (Button) findViewById(R.id.btn_createbase);
		btn.setEnabled(enabled);
		
		btn = (Button) findViewById(R.id.btn_destroybase);
		btn.setEnabled(enabled);
		
		btn = (Button) findViewById(R.id.btn_identitymatch);
		btn.setEnabled(enabled);
		
		btn = (Button) findViewById(R.id.btn_adduser);
		btn.setEnabled(enabled);
		
		btn = (Button) findViewById(R.id.btn_verifymatch);
		btn.setEnabled(enabled);
		
		btn = (Button) findViewById(R.id.btn_rebootsoft);
		btn.setEnabled(enabled);
		
		if (menuItemMSOConfiguration != null) {
			menuItemMSOConfiguration.setEnabled(enabled);
		}
		if (menuItemLoggingParameters != null) {
			menuItemLoggingParameters.setEnabled(enabled);
		}

		if (menuItemuserAreaData != null) {
			menuItemuserAreaData.setEnabled(enabled);
		}
		if (menuItemLoadKsUnsecureMode != null) {
			menuItemLoadKsUnsecureMode.setEnabled(enabled);
		}
		if (menuItemLoadKsAsymetricSecureMode != null) {
			menuItemLoadKsAsymetricSecureMode.setEnabled(enabled);
		}
		if (menuItemLoadKsSymetricSecureMode != null) {
			menuItemLoadKsSymetricSecureMode.setEnabled(enabled);
		}
		if (menuItemGetKs != null) {
			menuItemGetKs.setEnabled(enabled);
		}
		if (menuItemEnableBioDataEncryption != null) {
			menuItemEnableBioDataEncryption.setEnabled(enabled);
		}
		if (menuItemDecryptBioData != null) {
			menuItemDecryptBioData.setEnabled(enabled);
		}
		if (menuItemEncryptAESBioData != null) {
			menuItemEncryptAESBioData.setEnabled(enabled);
		}
		if (menuItemDecryptAESBioData != null) {
			menuItemDecryptAESBioData.setEnabled(enabled);
		}
		/*if (menuItemPrivacyMode != null && menuItemPrivacyMode.isVisible()) {
			menuItemPrivacyMode.setEnabled(enabled);
		}*/
		if (menuItemLoadKprivacyUnsecureMode != null) {
			menuItemDecryptAESBioData.setEnabled(enabled);
		}
		if (menuItemLoadKprivacySymmetricSecureMode != null) {
			menuItemLoadKprivacySymmetricSecureMode.setEnabled(enabled);
		}
		if (menuItemGetprivacyKcv != null) {
			menuItemGetprivacyKcv.setEnabled(enabled);
		}
	}
	
	public final void onStartStop(View view)
	{
		if (ProcessInfo.getInstance().isStarted())
		{			
			this.stop(true);
		}
		else
		{			
			MorphoTabActivity current = (MorphoTabActivity) getLocalActivityManager().getCurrentActivity();

			if (current.getClass() == IdentifyActivity.class)
			{
				if (ProcessInfo.getInstance().getDatabaseItems().size() == 0)
				{					
					alert(ErrorCodes.MORPHOERR_DB_EMPTY, 0, "Identify", "");
					return;
				}
				else
				{
					enableDisableIHM(false);
				}
			}

			if ((current.getClass() == CaptureActivity.class) || (current.getClass() == VerifyActivity.class) || (current.getClass() == EnrollActivity.class)
					|| (current.getClass() == IdentifyActivity.class) || (current.getClass() == FingerPrintActivity.class))
			{
				MorphoInfo info = current.retrieveSettings();
				if (info != null)
				{
					enableDisableIHM(false);
					Button startbutton = (Button) findViewById(R.id.startstop);
					ProcessInfo.getInstance().setMorphoInfo(info);
					ProcessInfo.getInstance().setMorphoSample(this);
					getTabHost().getTabWidget().getChildTabViewAt(PROCESS_TAB_INDEX).setBackgroundColor(Color.TRANSPARENT);
					switchTab(processTag);
					try
					{
						ProcessInfo.getInstance().setCommandBioStart(true);
						startbutton.setText(getResources().getString(R.string.stop));
						ProcessInfo.getInstance().setStarted(true);
						lockScreenOrientation();
					}
					catch (Exception e)
					{
					}

					getTabHost().getTabWidget().getChildTabViewAt(0).setEnabled(false);
					getTabHost().getTabWidget().getChildTabViewAt(1).setEnabled(false);
					getTabHost().getTabWidget().getChildTabViewAt(2).setEnabled(false);
					getTabHost().getTabWidget().getChildTabViewAt(3).setEnabled(false);
					getTabHost().getTabWidget().getChildTabViewAt(4).setEnabled(false);
					getTabHost().getTabWidget().getChildTabViewAt(5).setEnabled(false);
				}				
			}
		}
	}

	private void unlockScreenOrientation()
	{
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
	}

	private void lockScreenOrientation()
	{
		/* 20200305
		final int orientation = getResources().getConfiguration().orientation;
		final int rotation = getWindowManager().getDefaultDisplay().getOrientation();

		if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_90)
		{
			if (orientation == Configuration.ORIENTATION_PORTRAIT)
			{
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			}
			else if (orientation == Configuration.ORIENTATION_LANDSCAPE)
			{
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			}
		}
		else if (rotation == Surface.ROTATION_180 || rotation == Surface.ROTATION_270)
		{
			if (orientation == Configuration.ORIENTATION_PORTRAIT)
			{
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);
			}
			else if (orientation == Configuration.ORIENTATION_LANDSCAPE)
			{
				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);
			}
		}
		*/
	}

	public void switchTab(String tag)
	{
		getTabHost().setCurrentTabByTag(tag);
	}

	public void onIdentityMatchClick(View view)
	{
		currentAction = MorphoSampleAction.onIdentityMatchClick;
		try
		{
			Intent activityIntent = new Intent(this, FileChooserActivity.class);
			// Store the Workflow file in the intent
			startActivityForResult(activityIntent, 0);

		}
		catch (Exception e)
		{
		}
	}

	public void onVerifyMatchClick(View view)
	{
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
		alertDialog.setMessage(getString(R.string.message_choise_verify_first));
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				currentAction = MorphoSampleAction.onVerifyMatchClick;
				try
				{
					Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
					// Store the Workflow file in the intent
					startActivityForResult(activityIntent, 0);

				}
				catch (Exception e)
				{
				}
			}
		});
		alertDialog.show();
	}

	public void onAddUserClick(View view)
	{
		currentAction = MorphoSampleAction.onAddUserClick;
		try
		{
			Intent activityIntent = new Intent(this, FileChooserActivity.class);
			// Store the Workflow file in the intent
			startActivityForResult(activityIntent, 0);

		}
		catch (Exception e)
		{
		}
	}
	
	
	/**
	 * Function called when the FileChooserActivity exits!
	 * 
	 * @param requestCode
	 *            : Activity result request code
	 * @param resultCode
	 *            : how did the activity end (Next, Cancel, Back, ...) ?
	 * @param data
	 *            : Activity result data
	 * @since 1.0
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		int ret = ErrorCodes.MORPHO_OK;
		byte[] external_key = null;
		byte[] userAreaData = null;
		byte[] private_key = null;
		byte[] host_certificate = null;
		
		try {
			super.onActivityResult(requestCode, resultCode, data);
			if (RESULT_OK == resultCode) {
				Bundle b = data.getExtras();
				if (b.containsKey("SelectedFile")) {
					switch (currentAction) {
						case onGetAesKeyLoad:
							selected_file = b.getString("SelectedFile");
							byte[] privacyKey = MorphoTools.ReadFile(new File(selected_file)).toByteArray();							
							if (privacyKey == null || privacyKey.length != MsoSecu.AES128_BLOCK_LENGTH) { // AES 128 bits key length
								alert(getResources().getString(R.string.invalidAesKey));
							} else {
								ProcessInfo.getInstance().setPrivacyKey(Arrays.copyOf(privacyKey, privacyKey.length));
							}							
							// Set event to unblock getPrivacyKey()
							handler.sendMessage(handler.obtainMessage());
						break;
						case onAddUserClick:
							addUser(b.getString("SelectedFile"));
							break;
						case onIdentityMatchClick:
							identityMatch(b.getString("SelectedFile"));
							break;
						case onVerifyMatchClick:
							nbFile++;
							if (nbFile == 2) {
								nbFile = 0;
								template2 = b.getString("SelectedFile");
								verifyMatch(template1, template2);
							} else {
								template1 = b.getString("SelectedFile");
								AlertDialog alertDialog = new AlertDialog.Builder(this).create();
								alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
								alertDialog.setMessage(getString(R.string.message_choise_verify_second));
								alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										currentAction = MorphoSampleAction.onVerifyMatchClick;
										try {
											Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
											// Store the Workflow file in the intent
											startActivityForResult(activityIntent, 0);

										} catch (Exception e) {
										}
									}
								});
								alertDialog.show();
							}
							break;
						case onLoadKsUnsecureMode:
							// Read selected key
							external_key = MorphoTools.ReadFile(new File(b.getString("SelectedFile"))).toByteArray();
							// Load key on device
							ret = morphoDevice.loadKs(external_key);
							alert(ret, morphoDevice.getInternalError(), getResources().getString(R.string.loadKsUnsecureMode), "");
							break;
						case onLoadKsAsymetricSecureMode:
							// Read selected key
							external_key = MorphoTools.ReadFile(new File(b.getString("SelectedFile"))).toByteArray();
							// Read device certificate
							ArrayList<Byte> certificate = new ArrayList<Byte>();
							ret = morphoDevice.SecuReadCertificate(0, certificate);
							if (ErrorCodes.MORPHO_OK != ret) {
								alert(ret, morphoDevice.getInternalError(), getResources().getString(R.string.Error),  getResources().getString(R.string.loadCertificateError));
							} else {
								// Get private key /sdcard/Keys/host.key
								private_key = MorphoTools.ReadFile(new File(AppContext.RootPath+"Keys/host.key")).toByteArray();
								// Get host certificate /sdcard/Keys/host.der
								host_certificate = MorphoTools.ReadFile(new File(AppContext.RootPath+"Keys/host.der")).toByteArray();
								// Encrypt selected key with device certificate
								ArrayList<Byte> crypted_key = new ArrayList<Byte>();
								if (0 != msoSecu.rsaEncrypt(MorphoTools.toByteArray(certificate), external_key, crypted_key)) {
									alert(getResources().getString(R.string.rsaEncryptionError));
									return;
								}
								// Sign encrypted key
								ArrayList<Byte> crypted_key_signature = new ArrayList<Byte>();
								if (0 != msoSecu.signRSA(private_key, MorphoTools.toByteArray(crypted_key), crypted_key_signature)) {
									alert(getResources().getString(R.string.rsaSignError));
									return;
								}
								// Load key on device
								ret = morphoDevice.loadKsSecurely(MorphoTools.toByteArray(crypted_key), MorphoTools.toByteArray(crypted_key_signature), host_certificate);
								alert(ret, morphoDevice.getInternalError(), getResources().getString(R.string.loadKsAsymetricSecureMode), "");
							}
							break;
						case onLoadKsSymetricSecureMode:
							nbFile++;
							if (nbFile == 2) {
								nbFile = 0;
								// Get second selected file (new key)
								new_key = MorphoTools.ReadFile(new File(b.getString("SelectedFile"))).toByteArray();
								if (new_key == null || new_key.length != MsoSecu.TRIPLE_DES_KEY_LENGTH) { // Triple DES key length
									alert(getResources().getString(R.string.invalidKey));
									return;
								}
								// Encrypt [current_key|new_key] with current_key
								byte[] key = new byte[current_key.length+new_key.length];
								System.arraycopy(current_key, 0, key, 0, current_key.length);
								System.arraycopy(new_key, 0, key, current_key.length, new_key.length);
								byte[] iv = new byte[8]; // DES block length
								byte[] encrypted_key = new byte[key.length];
								if (0 != msoSecu.encrypt3DesCbcNopad(current_key, key, iv, encrypted_key)) {
									alert(getResources().getString(R.string.tripleDESEncryptionFailed));
									return;
								}
								// Load [iv|encrypted_key] in device
								byte[] iv_encrypted_key = new byte[8+encrypted_key.length];
								System.arraycopy(iv, 0, iv_encrypted_key, 0, 8);
								System.arraycopy(encrypted_key, 0, iv_encrypted_key, 8, encrypted_key.length);
								ret = morphoDevice.loadKsSecurely(iv_encrypted_key);
								alert(ret, morphoDevice.getInternalError(), getResources().getString(R.string.loadKsSymetricSecureMode), "");								
							} else {
								// Get first selected file (current key)
								current_key = MorphoTools.ReadFile(new File(b.getString("SelectedFile"))).toByteArray();
								if (current_key == null || current_key.length != MsoSecu.TRIPLE_DES_KEY_LENGTH) { // Triple DES key length
									alert(getResources().getString(R.string.invalidKey));
									nbFile = 0;
									return;
								}
								// Show second file selection dialog
								AlertDialog alertDialog = new AlertDialog.Builder(this).create();
								alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
								alertDialog.setMessage(getString(R.string.selectNewKey));
								alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										try {
											Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
											startActivityForResult(activityIntent, 0);

										} catch (Exception e) {
										}
									}
								});
								alertDialog.show();
							}
							break;
						case onDecryptBioData:
							nbFile++;
							if (nbFile == 2) {
								nbFile = 0;
								// Get second selected file (encryption key)
								final byte[] key = MorphoTools.ReadFile(new File(b.getString("SelectedFile"))).toByteArray();
								final byte[] crypted_file = MorphoTools.ReadFile(new File(selected_file)).toByteArray();
								// Display popup to enter diversification data length
								LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
								View layout = inflater.inflate(R.layout.set_diversification_data, (ViewGroup) findViewById(R.id.set_diversification_data));
								final PopupWindow pw = new PopupWindow(layout, ListPopupWindow.WRAP_CONTENT, ListPopupWindow.WRAP_CONTENT, true);
								pw.showAtLocation(layout, Gravity.CENTER, 0, 0);
								Button validationButton = (Button) layout.findViewById(R.id.validation_btn);
								validationButton.setOnClickListener(new View.OnClickListener() {
					                @Override
					                public void onClick(View v) {
					                	EditText diversification_data_edit_txt = (EditText)pw.getContentView().findViewById(R.id.diversification_data_txt);
					                	String diversification_data = diversification_data_edit_txt.getText().toString();
					                	if (!diversification_data.isEmpty()) {
					                		try {
					                			// Select consistent data to be decrypted
						                		byte[] decrypted_data = new byte[crypted_file.length-(crypted_file.length%8)];
												byte[] crypted_buffer = Arrays.copyOf(crypted_file, decrypted_data.length);
												// Decryption algo
												byte[] iv = new byte[]{0, 0, 0, 0, 0, 0, 0, 0};
												Cipher cipher = Cipher.getInstance("DESede/CBC/NoPadding");
												IvParameterSpec IV = new IvParameterSpec(iv);
												SecretKey secretKey = new SecretKeySpec(key, 0, key.length, "DESede");
												cipher.init(Cipher.DECRYPT_MODE, secretKey, IV);
												decrypted_data = cipher.doFinal(crypted_buffer);
												// Retrieve consistent data
												byte[] consistent_data = new byte[crypted_file.length];
												System.arraycopy(decrypted_data, 0, consistent_data, 0, decrypted_data.length);
												System.arraycopy(crypted_file, decrypted_data.length, consistent_data, decrypted_data.length, crypted_file.length%8);
												byte[] decrypted_diversification_data = new byte[diversification_data.length()];
												System.arraycopy(decrypted_data, 8, decrypted_diversification_data, 0, decrypted_diversification_data.length);
												byte[] decrypted_bio_data = Arrays.copyOfRange(consistent_data, 8+decrypted_diversification_data.length, consistent_data.length);
												// Verify diversification data
												if (!Arrays.equals(diversification_data.getBytes(), decrypted_diversification_data)) {
													alert(getResources().getString(R.string.decryptionDoesNotMatch));
												} else {
													// Save decrypted data on disk
													String[] crypted_file_splitted = selected_file.split("\\.");
													String output_file = crypted_file_splitted[0] + "_decrypted." + crypted_file_splitted[1];
													FileOutputStream fos = new FileOutputStream(output_file);
													fos.write(decrypted_bio_data);
													fos.close();
													String message = String.format(getResources().getString(R.string.decryptedDataOk), output_file);
													alert(message);
												}
					                		} catch (Exception e) {
												alert(e.getMessage());
											} 
					                	}
					                	pw.dismiss();
					                }
								});
								Button cancelButton = (Button) layout.findViewById(R.id.cancel_btn);
					            cancelButton.setOnClickListener(new View.OnClickListener() {
					                public void onClick(View v) {
					                    pw.dismiss();
					                }
					            });
							} else {
								// Get first selected file (crypted file)
								selected_file = b.getString("SelectedFile");
								// Show second file selection dialog
								AlertDialog alertDialog = new AlertDialog.Builder(this).create();
								alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
								alertDialog.setMessage(getString(R.string.selectExternalKey));
								alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										try {
											Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
											startActivityForResult(activityIntent, 0);

										} catch (Exception e) {
										}
									}
								});
								alertDialog.show();
							}
							break;
						case onAesEncrypt:
							nbFile++;
							if (nbFile == 2) {
								nbFile = 0;
								String clearFile = b.getString("SelectedFile");
								final byte[] clear_data = MorphoTools.ReadFile(new File(clearFile)).toByteArray();
								final byte[] key = MorphoTools.ReadFile(new File(selected_file)).toByteArray();
								
								if (key == null || key.length != MsoSecu.AES128_BLOCK_LENGTH) { // AES 128 bits key length
									alert(getResources().getString(R.string.invalidAesKey));
									return;
								}
														
								try {
									byte[] iv = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
									ArrayList<Byte> aesEncryptedData = new ArrayList<Byte>();
									
									int error = msoSecu.encryptAes128Cbc(key, clear_data, iv, true, aesEncryptedData);
									if (error != 0) {
										String message = String.format(getResources().getString(R.string.aesError), error);
										alert(message);
									} else {
										byte[] crypted_bio_data = MorphoTools.toByteArray(aesEncryptedData);
										
										String[] crypted_file_splitted = clearFile.split("\\.");
										String output_file = crypted_file_splitted[0] + "_crypted." + crypted_file_splitted[1];
										
										FileOutputStream fos = new FileOutputStream(output_file);
										fos.write(crypted_bio_data);
										fos.close();
										String message = String.format(getResources().getString(R.string.aesEncryptionOK), output_file);
										alert(message);
									}
		                		} catch (Exception e) {
									alert(e.getMessage());
								} 
							} else {
								selected_file = b.getString("SelectedFile");

								AlertDialog alertDialog = new AlertDialog.Builder(this).create();
								alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
								alertDialog.setMessage(getString(R.string.selectDataToCrypt));
								alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										try {
											Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
											startActivityForResult(activityIntent, 0);
										} catch (Exception e) {
										}
									}
								});
								alertDialog.show();
								break;
							} 
							break;
						case onAesDecrypt:
							nbFile++;
							if(nbFile == 2) {
								nbFile = 0;

								String cryptedFile = b.getString("SelectedFile");
								final byte[] crypted_file = MorphoTools.ReadFile(new File(cryptedFile)).toByteArray();
								final byte[] key = MorphoTools.ReadFile(new File(selected_file)).toByteArray();
								
								if (key == null || key.length != MsoSecu.AES128_BLOCK_LENGTH) { // AES 128 bits key length
									alert(getResources().getString(R.string.invalidAesKey));
									return;
								}

								try {
									byte[] iv = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
									
									ArrayList<Byte> aesDecryptedData = new ArrayList<Byte>();
									
									int error = msoSecu.decryptAes128Cbc(key, crypted_file, iv, true, aesDecryptedData);
									
									byte[] decrypted_bio_data = new byte[aesDecryptedData.size()];
									for(int i = 0; i < decrypted_bio_data.length; ++i) {
										decrypted_bio_data[i] = aesDecryptedData.get(i).byteValue();
									}
									
									if (error != 0) {
										String message = String.format(getResources().getString(R.string.aesError), error);
										alert(message);
										
									} else {
										String[] crypted_file_splitted = cryptedFile.split("\\.");
										String output_file = crypted_file_splitted[0] + "_decrypted." + crypted_file_splitted[1];
																				
										FileOutputStream fos = new FileOutputStream(output_file);
										fos.write(decrypted_bio_data);
										fos.close();
										String message = String.format(getResources().getString(R.string.aesDecryptionOK), output_file);
										alert(message);
									}
		                		} catch (Exception e) {
									alert(e.getMessage());
								} 
								
							} else {
								selected_file = b.getString("SelectedFile");
								
								AlertDialog alertDialog = new AlertDialog.Builder(this).create();
								alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
								alertDialog.setMessage(getString(R.string.selectDataToDecrypt));
								alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										try {
											Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
											startActivityForResult(activityIntent, 0);
										} catch (Exception e) {
										}
									}
								});
								alertDialog.show();
								break;
							}
							break;
						case onLoadKprivacyUnsecureMode:
							// Read selected key
							external_key = MorphoTools.ReadFile(new File(b.getString("SelectedFile"))).toByteArray();
							if (external_key == null || external_key.length != MsoSecu.AES128_BLOCK_LENGTH) { // AES 128 key length
								alert(getResources().getString(R.string.invalidAesKey));
								nbFile = 0;
								return;
							}
							// Load key on device
							ret = morphoDevice.loadKprivacy(external_key);
							alert(ret, morphoDevice.getInternalError(), getResources().getString(R.string.loadKprivacyUnsecureMode), "");
							break;
						case onWriteUserAreaData:
							// Read data to set to UserAreaData
							userAreaData = MorphoTools.ReadFile(new File(b.getString("SelectedFile"))).toByteArray();
							if (userAreaData == null || userAreaData.length >= 2048) { // invalid  userAreaData
								alert(getResources().getString(R.string.invaliduserAreadata));
								nbFile = 0;
								return;
							}
							// Write user data  on device
							ret = morphoDevice.setUserAreaData(userAreaData);
							alert(ret, morphoDevice.getInternalError(), getResources().getString(R.string.WriteDataUserArea), "");
							break;
						case onLoadKprivacySymmetricSecureMode:
							nbFile++;
							if (nbFile == 2) {
								nbFile = 0;
								// Get second selected file (new key)
								new_key = MorphoTools.ReadFile(new File(b.getString("SelectedFile"))).toByteArray();
								if (new_key == null || new_key.length != MsoSecu.AES128_BLOCK_LENGTH) { // AES 128 key length
									alert(getResources().getString(R.string.invalidAesKey));
									return;
								}
								// Encrypt [new_key] with current_key (do not use padding)
				
								try{		
								byte[] iv = new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
								ArrayList<Byte> keyPrivacycryptedData = new ArrayList<Byte>();
								int error = msoSecu.encryptAes128Cbc(current_key, new_key, iv, false, keyPrivacycryptedData);
								if (error != 0) {
									String message = String.format(getResources().getString(R.string.aesError), error);
									alert(message);
									return ;
								}
								
								//get the Privacy Seed from the device
								byte[] seedData = null;
								seedData = morphoDevice.getPrivacySeed();
								if (seedData == null) {
									alert(getResources().getString(R.string.getPrivacySeedError));
									return ;
								}
								
								// encrypt the seed with current key
								ArrayList<Byte> cryptogramcryptedData = new ArrayList<Byte>();
								error = msoSecu.encryptAes128Cbc(current_key, seedData, MorphoTools.toByteArray(keyPrivacycryptedData),true, cryptogramcryptedData);
						
								if (error != 0) {
									String message = String.format(getResources().getString(R.string.aesError), error);
									alert(message);
									return ;
								}
								// load new key in device: key is sent in ciphertext
								ret = morphoDevice.loadKprivacySecurely(MorphoTools.toByteArray(keyPrivacycryptedData),MorphoTools.toByteArray(cryptogramcryptedData));
								alert(ret, morphoDevice.getInternalError(), getResources().getString(R.string.loadKsSymetricSecureMode), "");								
							
								}catch (Exception e) {
								alert(e.getMessage());
							}
								
							} else {
								// Get first selected file (current key)
								current_key = MorphoTools.ReadFile(new File(b.getString("SelectedFile"))).toByteArray();
								if (current_key == null || current_key.length != MsoSecu.AES128_BLOCK_LENGTH) { // AES 128 key length
									alert(getResources().getString(R.string.invalidAesKey));
									nbFile = 0;
									return;
								}
								// Show second file selection dialog
								AlertDialog alertDialog = new AlertDialog.Builder(this).create();
								alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
								alertDialog.setMessage(getString(R.string.selectNewKey));
								alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										try {
											Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
											startActivityForResult(activityIntent, 0);

										} catch (Exception e) {
										}
									}
								});
								alertDialog.show();
							}
							break;
					default:
						break;
					}
				}
			}
		}
		catch (Exception e) {
			alert(e.getMessage());
		}
	}

	private void verifyMatch(String file1, String file2)
	{
		try
		{
			TemplateList templateListSearch = new TemplateList();
			TemplateList templateListReference = new TemplateList();
			byte[] random = new byte[4];
			new Random().nextBytes(random);
			ITemplateType iTemplateType = ProcessActivity.getTemplateTypeFromExtention(ProcessActivity.getFileExtension(file1));
			if (iTemplateType != TemplateType.MORPHO_NO_PK_FP)
			{
				DataInputStream dis = new DataInputStream(new FileInputStream(file1));
				int length = dis.available();
				byte[] buffer = new byte[length];
				dis.readFully(buffer);

				if (iTemplateType instanceof TemplateType)
				{
					Template template = new Template();
					if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {
						byte[] privacyKey = null;
						privacyKey = ProcessInfo.getInstance().getPrivacyKey();
						if (null == privacyKey) {
							alert("An error occured while getting Privacy Key");
							dis.close();
							return;
						}
						
						byte[] templateCrypted = encryptPrivacyData(buffer,privacyKey, random);
						template.setData(templateCrypted);
					} else {
						template.setData(buffer);
					}
					
					template.setTemplateType((TemplateType) iTemplateType);
					templateListSearch.putTemplate(template);
				}
				else
				{
					TemplateFVP template = new TemplateFVP();
					if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {
						byte[] privacyKey = null;
						privacyKey = ProcessInfo.getInstance().getPrivacyKey();
						if (null == privacyKey) {
							alert("An error occured while getting Privacy Key");
							dis.close();
							return;
						}
						
						byte[] templateCrypted = encryptPrivacyData(buffer,privacyKey, random);
						template.setData(templateCrypted);
					} else {
						template.setData(buffer);
					}
					template.setTemplateFVPType((TemplateFVPType) iTemplateType);
					templateListSearch.putFVPTemplate(template);
				}
				dis.close();
			}
			else
			{
				alert(file1 + " not valide!!");
				return;
			}

			iTemplateType = ProcessActivity.getTemplateTypeFromExtention(ProcessActivity.getFileExtension(file2));
			if (iTemplateType != TemplateType.MORPHO_NO_PK_FP)
			{
				DataInputStream dis = new DataInputStream(new FileInputStream(file2));
				int length = dis.available();
				byte[] buffer = new byte[length];
				dis.readFully(buffer);

				if (iTemplateType instanceof TemplateType)
				{
					Template template = new Template();
					if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {
						byte[] privacyKey = null;
						privacyKey = ProcessInfo.getInstance().getPrivacyKey();
						if (null == privacyKey) {
							alert("An error occured while getting Privacy Key");
							dis.close();
							return;
						}
						
						byte[] templateCrypted = encryptPrivacyData(buffer,privacyKey, random);
						template.setData(templateCrypted);
					} else {
						template.setData(buffer);
					}
					template.setTemplateType((TemplateType) iTemplateType);
					templateListReference.putTemplate(template);
				}
				else
				{
					TemplateFVP template = new TemplateFVP();
					if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {
						byte[] privacyKey = null;
						privacyKey = ProcessInfo.getInstance().getPrivacyKey();
						if (null == privacyKey) {
							alert("An error occured while getting Privacy Key");
							dis.close();
							return;
						}
						
						byte[] templateCrypted = encryptPrivacyData(buffer,privacyKey, random);
						template.setData(templateCrypted);
					} else {
						template.setData(buffer);
					}
					template.setTemplateFVPType((TemplateFVPType) iTemplateType);
					templateListReference.putFVPTemplate(template);
				}
				dis.close();
			}
			else
			{
				alert(file2 + " not valide!!");
				return;
			}

			int far = ProcessInfo.getInstance().getMatchingThreshold();
			Integer matchingScore = null;
			// Check if running under security mode
			if (!isTunnelingMode && !isOfferedSecurityMode) {
				matchingScore = new Integer(0);
			}
			
			int ret = morphoDevice.verifyMatch(far, templateListSearch, templateListReference, matchingScore);
			String message = "";
			if (ret == 0 && matchingScore != null)
			{
				message = "Matching Score : " + matchingScore;
			}

			alert(ret, morphoDevice.getInternalError(), "Verify Match", message);

		}
		catch (IOException e)
		{
			alert(e.getMessage());
		}
	}

	private void identityMatch(String fileName)
	{
		try
		{
			DataInputStream dis = new DataInputStream(new FileInputStream(fileName));
			int length = dis.available();
			byte[] buffer = new byte[length];
			byte[] random = new byte[4];
			new Random().nextBytes(random);
			dis.readFully(buffer);
			TemplateList templateList = new TemplateList();
			Template template = new Template();
			TemplateFVP templateFVP = new TemplateFVP();
			ITemplateType iTemplateType = ProcessActivity.getTemplateTypeFromExtention(ProcessActivity.getFileExtension(fileName));
			if (iTemplateType != TemplateType.MORPHO_NO_PK_FP)
			{
				if (iTemplateType instanceof TemplateType)
				{
					if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {
						byte[] privacyKey = null;
						privacyKey = ProcessInfo.getInstance().getPrivacyKey();
						if (null == privacyKey) {
							alert("An error occured while getting Privacy Key");
							dis.close();
							return;
						}
						
						byte[] templateCrypted = encryptPrivacyData(buffer,privacyKey, random);
						template.setData(templateCrypted);
					} else {
						template.setData(buffer);
					}
					template.setTemplateType((TemplateType) iTemplateType);
					templateList.putTemplate(template);
				}
				else
				{
					if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {
						byte[] privacyKey = null;
						privacyKey = ProcessInfo.getInstance().getPrivacyKey();
						if (null == privacyKey) {
							alert("An error occured while getting Privacy Key");
							dis.close();
							return;
						}
						
						byte[] templateCrypted = encryptPrivacyData(buffer,privacyKey, random);
						templateFVP.setData(templateCrypted);
					} else {
					templateFVP.setData(buffer);
					}
					templateFVP.setTemplateFVPType((TemplateFVPType) iTemplateType);
					templateList.putFVPTemplate(templateFVP);
				}
				dis.close();
			}
			else
			{
				alert(fileName + " not valide!!");
				dis.close();
				return;
			}

			dis.close();

			final MorphoUser morphoUser = new MorphoUser();
			int far = ProcessInfo.getInstance().getMatchingThreshold();
			ResultMatching resultMatching = null;
			// Check if running under security mode
			if (!isTunnelingMode && !isOfferedSecurityMode) {
				resultMatching = new ResultMatching();
				resultMatching.setMatchingScore(0);
				resultMatching.setMatchingPKNumber(255);
			}
			
			int ret = morphoDatabase.identifyMatch(far, templateList, morphoUser, resultMatching);
			if (ret == 0)
			{
				if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus())	{
					final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
					alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
					alertDialog.setMessage("Decrypt User info : ");
					alertDialog.setCancelable(false);
					alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener()
					{
						public void onClick(DialogInterface dialog, int which)
						{	
							byte[] 	userIDB = morphoUser.getBufferField(0);
							byte[]  firstNameB = morphoUser.getBufferField(1);
							byte[]  lastNameB = morphoUser.getBufferField(2);
						
							String userID = new String(userIDB);
							String firstName= new String(firstNameB);
							String lastName= new String(lastNameB);
							
							String message = "User identified";
							message += "\r\nUser ID   : \t\t" + userID;
							message += "\r\nFirstName : \t\t" + firstName;
							message += "\r\nLastName  : \t\t" + lastName;
							alert(0, morphoDevice.getInternalError(), "Identify Match", message);
						}
					});
					alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener()
					{
						public void onClick(DialogInterface dialog, int which)
						{	
							byte[] 	userIDB = morphoUser.getBufferField(0);
							byte[]  firstNameB = morphoUser.getBufferField(1);
							byte[]  lastNameB = morphoUser.getBufferField(2);
							
							byte[] privacyKey = ProcessInfo.getInstance().getPrivacyKey();
							if (null != privacyKey) {
								String userID = "";
								String firstName = "";
								String lastName = "";
								
								byte[] decrypted_bio_data =	decryptAndCheckPrivacyData(userIDB,privacyKey);
								if (null != decrypted_bio_data)
									userID = new String(decrypted_bio_data);
								
								decrypted_bio_data = decryptAndCheckPrivacyData(firstNameB,privacyKey);
								if (null != decrypted_bio_data)
									firstName = new String(decrypted_bio_data);
								
								decrypted_bio_data = decryptAndCheckPrivacyData(lastNameB,privacyKey);
								if (null != decrypted_bio_data)
									lastName = new String(decrypted_bio_data);
								
								String message = "User identified";
								message += "\r\nUser ID   : \t\t" + userID;
								message += "\r\nFirstName : \t\t" + firstName;
								message += "\r\nLastName  : \t\t" + lastName;
								alert(0, morphoDevice.getInternalError(), "Identify Match", message);
							}
						};
					});
					alertDialog.show();
					
				} else {
					String userID = morphoUser.getField(0);
					String firstName = morphoUser.getField(1);
					String lastName = morphoUser.getField(2);
					String message = "User identified";
					message += "\r\nUser ID   : \t\t" + userID;
					message += "\r\nFirstName : \t\t" + firstName;
					message += "\r\nLastName  : \t\t" + lastName;
					alert(ret, morphoDevice.getInternalError(), "Identify Match", message);
				}
			} else {
				alert(ret, morphoDevice.getInternalError(), "Identify Match", "");
			}
		} catch (FileNotFoundException e) {
			alert(e.getMessage());
		} catch (IOException e) {
			alert(e.getMessage());
		}
	}

	public void addUser(String fileName)
	{
		try
		{
			DataInputStream dis = new DataInputStream(new FileInputStream(fileName));
			int length = dis.available();
			final byte[] buffer = new byte[length];
			dis.readFully(buffer);
			dis.close();
			final Template template = new Template();
			final TemplateFVP templateFVP = new TemplateFVP();

			//Construct the TemplateType
			final ITemplateType iTemplateType = ProcessActivity.getTemplateTypeFromExtention(ProcessActivity.getFileExtension(fileName));
			// Set the interface TemplateType as TemplateFVPType if template is a FVP type
			// else set it to simple PK template
			if (iTemplateType != TemplateType.MORPHO_NO_PK_FP)
			{
				if (iTemplateType instanceof TemplateFVPType)
				{
					templateFVP.setTemplateFVPType((TemplateFVPType) iTemplateType);
				}
				else
				{
					template.setTemplateType((TemplateType) iTemplateType);
				}
			}
			else
			{
				alert(fileName + " not valid !!");
				return;
			}
			
			LayoutInflater factory = LayoutInflater.from(this);
			final View textEntryView = factory.inflate(R.layout.user_information, null);
			final EditText idUser = (EditText) textEntryView.findViewById(R.id.idnumberUI);
			final EditText firstName = (EditText) textEntryView.findViewById(R.id.firstnameUI);
			final EditText lastName = (EditText) textEntryView.findViewById(R.id.lastnameUI);

			final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
			alertDialog.setMessage("Add User : ");
			alertDialog.setCancelable(false);
			alertDialog.setView(textEntryView);
			alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					byte[] privacyKey = null;
					byte[] random = new byte[4];
					new Random().nextBytes(random);
					MorphoUser morphoUser = new MorphoUser();
					int ret;
					String idUserCS = idUser.getText().toString();
					if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {
						privacyKey = getPrivacyKey();
						if (null == privacyKey) {
							alert("An error occured while getting Privacy Key");
							return;
						}
						byte[] idUserC = encryptPrivacyData(idUserCS.getBytes(),privacyKey, random);
					   	ret = morphoDatabase.getUserBuffer(idUserC, morphoUser);
					} else {
						ret = morphoDatabase.getUser(idUser.getText().toString(), morphoUser);
					}
					 
					if (ret == 0) {
						if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) { 
							String firstNameS = firstName.getText().toString();
							byte[] firstNameCrypted = encryptPrivacyData(firstNameS.getBytes(),privacyKey, random);
							ret = morphoUser.putBufferField(1, firstNameCrypted);
						 } else {
							 ret = morphoUser.putField(1, MorphoTools.checkfield(firstName.getText().toString(),false));
						 }
					}

					if (ret == 0) {
						if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {								
							String lastNameS = lastName.getText().toString();
							byte[] lastNameCrypted = encryptPrivacyData(lastNameS.getBytes(),privacyKey, random);
							 ret = morphoUser.putBufferField(2, lastNameCrypted);
						 } else {
							 ret = morphoUser.putField(2, MorphoTools.checkfield(lastName.getText().toString(),false));
						 }
					}
					String additionalMessage = "";
					if (ret == 0) {
						Integer index = new Integer(0);

						if (iTemplateType instanceof TemplateType) {	
							if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {	
								byte[] templateCrypted = encryptPrivacyData(buffer,privacyKey, random);
								template.setData(templateCrypted);
							} else {
								template.setData(buffer);
							}
							
							morphoUser.putTemplate(template, index);
						} else {
							if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {	
								byte[] templateCrypted = encryptPrivacyData(buffer,privacyKey, random);
								template.setData(templateCrypted);
							} else {
								templateFVP.setData(buffer);
							}
							
							morphoUser.putFVPTemplate(templateFVP, index);
						}
					}
					ProcessInfo processInfo = ProcessInfo.getInstance();
					if (processInfo.isNoCheck()) {
						morphoUser.setNoCheckOnTemplateForDBStore(true);
					}
					
					ret = morphoUser.dbStore();
					if (ret == 0) {
						DatabaseItem databaseItemsItem = new DatabaseItem(idUser.getText().toString(), MorphoTools.checkfield(firstName.getText().toString(),false), MorphoTools.checkfield(lastName.getText().toString(),false));
						List<DatabaseItem> databaseItems = processInfo.getDatabaseItems();
						databaseItems.add(databaseItemsItem);
						processInfo.setDatabaseItems(databaseItems);
						refreshNbrOfUsedRecord();
						loadDatabaseItem();
					}					
					
					alert(ret, morphoDevice.getInternalError(), "Add User", additionalMessage);
				}
			});
			alertDialog.show();

		}
		catch (FileNotFoundException e)
		{
			alert(e.getMessage());
		}
		catch (IOException e)
		{
			alert(e.getMessage());
		}
	}

	public void onUpdateUserClick(View view)
	{
		if (ProcessInfo.getInstance().getDatabaseSelectedIndex() != -1)
		{
			LayoutInflater factory = LayoutInflater.from(this);
			final View textEntryView = factory.inflate(R.layout.user_information, null);
			final EditText idUser = (EditText) textEntryView.findViewById(R.id.idnumberUI);
			final EditText firstName = (EditText) textEntryView.findViewById(R.id.firstnameUI);
			final EditText lastName = (EditText) textEntryView.findViewById(R.id.lastnameUI);
			idUser.setEnabled(false);
			idUser.setText(ProcessInfo.getInstance().getDatabaseItems().get(ProcessInfo.getInstance().getDatabaseSelectedIndex()).getId());
			firstName.setText(ProcessInfo.getInstance().getDatabaseItems().get(ProcessInfo.getInstance().getDatabaseSelectedIndex()).getFirstName());
			lastName.setText(ProcessInfo.getInstance().getDatabaseItems().get(ProcessInfo.getInstance().getDatabaseSelectedIndex()).getLastName());

			final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
			alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
			alertDialog.setMessage("Update User : ");
			alertDialog.setCancelable(false);
			alertDialog.setView(textEntryView);	
			alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{

				}
			});
			alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
			{
				public void onClick(DialogInterface dialog, int which)
				{
					byte[] privacyKey = ProcessInfo.getInstance().getPrivacyKey();
					byte[] random = new byte[4];
					new Random().nextBytes(random);
					try	{
						int ret = 0;
						MorphoUser morphoUser = new MorphoUser();
						String idUserCS = idUser.getText().toString();
						if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {
							if (null == privacyKey) {
								alert("An error occured while getting Privacy Key");
								return;
							}
							byte[] idUserC = encryptPrivacyData(idUserCS.getBytes(), privacyKey, random);
							ret = morphoDatabase.getUserBuffer(idUserC, morphoUser);
						} else {
							ret = morphoDatabase.getUser(idUser.getText().toString(), morphoUser);
						}

						if (ret == 0) {
							 if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) { 
								 byte[] firstNameInBytes = firstName.getText().toString().getBytes();
								 int firstNameLength = firstNameInBytes.length;
								 byte[] data= new byte[firstNameLength+4+4];
									
								 byte [] indexInBytes= MorphoTools.longToFourByteBuffer(1,true);
								 byte [] firstNameLengthInBytes = MorphoTools.longToFourByteBuffer(firstNameLength,true);
								 
								 System.arraycopy(indexInBytes, 0, data, 0, 4);
								 System.arraycopy(firstNameLengthInBytes ,0, data ,4, 4);
								 System.arraycopy(firstNameInBytes, 0, data, 8, firstNameLength);

								 byte[] firstNameCrypted = encryptPrivacyData(data, privacyKey, random);
								 ret = morphoUser.putBufferField(1, firstNameCrypted);
							 } else {
								 ret = morphoUser.putField(1, MorphoTools.checkfield(firstName.getText().toString(),false));
							 }
						}

						if (ret == 0) {
							if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {								
								byte[] lastNameInBytes = lastName.getText().toString().getBytes();
								int lastNameLength = lastNameInBytes.length;
								byte[] data=new byte[lastNameLength+4+4];
								
								byte[] indexInBytes = MorphoTools.longToFourByteBuffer(2,true);
								byte [] lastNameLengthInBytes = MorphoTools.longToFourByteBuffer(lastNameLength,true);

								System.arraycopy(indexInBytes, 0, data, 0, 4);
								System.arraycopy(lastNameLengthInBytes, 0, data, 4, 4);
								System.arraycopy(lastNameInBytes, 0, data, 8, lastNameLength);
									
								byte[] lastNameC = encryptPrivacyData(data, privacyKey, random);		
								ret = morphoUser.putBufferField(2, lastNameC);
							 } else {
								 ret = morphoUser.putField(2, MorphoTools.checkfield(lastName.toString(),false));
							 }
						}

						ret = morphoUser.dbUpdatePublicFields();
						if (ret == 0) {
							DatabaseItem databaseItemsItem = new DatabaseItem(idUser.getText().toString(), MorphoTools.checkfield(firstName.getText().toString(),false), MorphoTools.checkfield(lastName.getText().toString(),false));
							List<DatabaseItem> databaseItems = ProcessInfo.getInstance().getDatabaseItems();
							databaseItems.remove(ProcessInfo.getInstance().getDatabaseSelectedIndex());
							databaseItems.add(ProcessInfo.getInstance().getDatabaseSelectedIndex(), databaseItemsItem);
							ProcessInfo.getInstance().setDatabaseItems(databaseItems);
							loadDatabaseItem();
						}
						alert(ret, morphoDevice.getInternalError(), "Update User", "");
					}
					catch (Exception e)
					{
						Log.e("ERROR", e.toString());
					}
					finally
					{
						ProcessInfo.getInstance().setDatabaseSelectedIndex(-1);
					}
				}
			});
			
			alertDialog.show();
		}
		else
		{
			alert(this.getResources().getString(R.string.selectuserfirst));
		}
	}

	public void onRemoveUserClick(View view)
	{
		try
		{
			if (ProcessInfo.getInstance().getDatabaseSelectedIndex() != -1)
			{
				MorphoUser morphoUser = new MorphoUser();
				ProcessInfo.getInstance().getDatabaseSelectedIndex();
				String userID = ProcessInfo.getInstance().getDatabaseItems().get(ProcessInfo.getInstance().getDatabaseSelectedIndex()).getId();
				
				int ret = ErrorCodes.MORPHO_OK;
			 
				if(MorphoDevicePrivacyModeStatus.PRIVACY_MODE_DISABLED != ProcessInfo.getInstance().getPrivacyModeStatus()) {
					byte[] random = new byte[4];
					new Random().nextBytes(random);
					byte[] privacyKey = ProcessInfo.getInstance().getPrivacyKey();
					if (null != privacyKey) {
						byte[] idUserCrypted = encryptPrivacyData(userID.getBytes(), privacyKey, random);
						ret = morphoDatabase.getUserBuffer(idUserCrypted, morphoUser);
					}
				} else {
					ret = morphoDatabase.getUser(userID, morphoUser);
				}
				
				if (ret == ErrorCodes.MORPHO_OK) {
					ret = morphoUser.dbDelete();
					alert(ret, morphoDevice.getInternalError(), "Remove User", "");
					if (ret == ErrorCodes.MORPHO_OK) {
						ProcessInfo.getInstance().getDatabaseItems().remove(ProcessInfo.getInstance().getDatabaseSelectedIndex());
						refreshNbrOfUsedRecord();
						loadDatabaseItem();
					}
				} else {
					alert(ret, morphoDevice.getInternalError(), "MorphoDatabase GetUser", "");
				}
			} else {
				alert(this.getResources().getString(R.string.selectuserfirst));
			}
		} catch (Exception e) {
			Log.e("ERROR", e.toString());
		} finally {
			ProcessInfo.getInstance().setDatabaseSelectedIndex(-1);
		}
	}

	public void activateButton(boolean isBaseAvailable, boolean isBaseNotEmpty)
	{
		Button btn = null;
		if(isBaseAvailable)
		{
			btn = (Button) findViewById(R.id.btn_createbase);
			btn.setEnabled(false);
			
			btn = (Button) findViewById(R.id.btn_adduser);
			btn.setEnabled(true);
			
			btn = (Button) findViewById(R.id.btn_destroybase);
			btn.setEnabled(true);
			
			if(isBaseNotEmpty)
			{
				btn = (Button) findViewById(R.id.btn_updateuser);
				btn.setEnabled(true);
				
				btn = (Button) findViewById(R.id.btn_removeuser);
				btn.setEnabled(true);
				
				btn = (Button) findViewById(R.id.btn_removeall);
				btn.setEnabled(true);
				
				btn = (Button) findViewById(R.id.btn_identitymatch);
				btn.setEnabled(true);
			}
			else
			{
				btn = (Button) findViewById(R.id.btn_updateuser);
				btn.setEnabled(false);
				
				btn = (Button) findViewById(R.id.btn_removeuser);
				btn.setEnabled(false);
				
				btn = (Button) findViewById(R.id.btn_removeall);
				btn.setEnabled(false);
				
				btn = (Button) findViewById(R.id.btn_identitymatch);
				btn.setEnabled(false);
			}
		}
		else
		{
			btn = (Button) findViewById(R.id.btn_createbase);
			btn.setEnabled(true);
			
			btn = (Button) findViewById(R.id.btn_updateuser);
			btn.setEnabled(false);
			
			btn = (Button) findViewById(R.id.btn_removeuser);
			btn.setEnabled(false);
			
			btn = (Button) findViewById(R.id.btn_removeall);
			btn.setEnabled(false);
			
			btn = (Button) findViewById(R.id.btn_identitymatch);
			btn.setEnabled(false);
			
			btn = (Button) findViewById(R.id.btn_adduser);
			btn.setEnabled(false);
			
			btn = (Button) findViewById(R.id.btn_destroybase);
			btn.setEnabled(false);
		}
	}

	public void onRemoveAllClick(View view)
	{
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
		alertDialog.setMessage(this.getResources().getString(R.string.eraseall));
		alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
			}
		});
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				int ret = morphoDatabase.dbDelete(MorphoTypeDeletion.MORPHO_ERASE_BASE);
				if (ret == 0)
				{
					ProcessInfo.getInstance().getDatabaseItems().clear();
					refreshNbrOfUsedRecord();
					loadDatabaseItem();
				}
				alert(ret, morphoDevice.getInternalError(), "Remove All", "");
			}
		});		
		alertDialog.show();
	}

	public void onCreateBaseClick(View view)
	{
		LayoutInflater factory = LayoutInflater.from(this);
		final View textEntryView = factory.inflate(R.layout.base_config, null);
		final EditText input1 = (EditText) textEntryView.findViewById(R.id.editTextMaximumnumberofrecord);
		final EditText input2 = (EditText) textEntryView.findViewById(R.id.editTextNumberoffingerperrecord);
		input1.setText("500");
		input2.setText("2");
		final RadioGroup radioEncryptDatabase = (RadioGroup) textEntryView.findViewById(R.id.radioEncryptDatabase);
		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
		alertDialog.setMessage("Data Base configuration : ");
		alertDialog.setCancelable(false);
		alertDialog.setView(textEntryView);
		alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{				
			}
		});
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				Integer index = new Integer(0);
				MorphoField morphoFieldFirstName = new MorphoField();
				morphoFieldFirstName.setName("First");
				morphoFieldFirstName.setMaxSize(15);
				morphoFieldFirstName.setFieldAttribute(FieldAttribute.MORPHO_PUBLIC_FIELD);
				morphoDatabase.putField(morphoFieldFirstName, index);
				MorphoField morphoFieldLastName = new MorphoField();
				morphoFieldLastName.setName("Last");
				morphoFieldLastName.setMaxSize(15);
				morphoFieldLastName.setFieldAttribute(FieldAttribute.MORPHO_PUBLIC_FIELD);
				morphoDatabase.putField(morphoFieldLastName, index);

				int maxRecord = Integer.parseInt(input1.getText().toString());
				int maxNbFinger = Integer.parseInt(input2.getText().toString());

				boolean encryptDatabase = false;
				
				if(radioEncryptDatabase.getCheckedRadioButtonId() == R.id.radioButtonencryptDatabaseYes)
				{
					encryptDatabase = true;
				}
				int ret = morphoDatabase.dbCreate(maxRecord, maxNbFinger, TemplateType.MORPHO_PK_COMP,0, encryptDatabase);
				if (ret == ErrorCodes.MORPHO_OK)
				{
					ProcessInfo.getInstance().setBaseStatusOk(true);
					initDatabaseStatus();

					Button btn = (Button) findViewById(R.id.btn_createbase);
					btn.setEnabled(false);
					btn = (Button) findViewById(R.id.btn_destroybase);
					btn.setEnabled(true);

					loadDatabaseItem();
					initDatabaseInformations();

					getTabHost().getTabWidget().getChildTabViewAt(3).setVisibility(View.VISIBLE);
					getTabHost().getTabWidget().getChildTabViewAt(4).setVisibility(View.VISIBLE);
				}
				alert(ret, morphoDevice.getInternalError(), "Create Base", "");
			}
		});		
		alertDialog.show();
	}

	private int destroyBase() {
				int ret = morphoDatabase.dbDelete(MorphoTypeDeletion.MORPHO_DESTROY_BASE);
		if (ret == 0) {
					ProcessInfo.getInstance().setBaseStatusOk(false);
					initDatabaseStatus();

					loadDatabaseItem();
					initDatabaseInformations();

					activateButton(false,false);
					Button btn = (Button) findViewById(R.id.btn_createbase);
					btn.setEnabled(true);
					btn = (Button) findViewById(R.id.btn_destroybase);
					btn.setEnabled(false);

					getTabHost().getTabWidget().getChildTabViewAt(3).setVisibility(View.GONE);
					getTabHost().getTabWidget().getChildTabViewAt(4).setVisibility(View.GONE);
					String currProcessTag = getTabHost().getCurrentTabTag();
					if (enrollTag.equals(currProcessTag) || identifyTag.equals(currProcessTag))
					{
						currProcessTag = captureTag;
					}
					switchTab(currProcessTag);
				}
		
		return ret;
	}
	
	public void onDestroyBaseClick(View view)
	{
		AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(this.getResources().getString(R.string.morphosample));
		alertDialog.setMessage(this.getResources().getString(R.string.destroyconfirm));
		alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{

			}
		});
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				int ret = destroyBase();
				alert(ret, morphoDevice.getInternalError(), "Destroy Base", "");
			}
		});		
		alertDialog.show();
	}

	private void hideLayout(int id, int button)
	{
		try
		{
			LinearLayout ll = (LinearLayout) findViewById(id);
			ll.setVisibility(LinearLayout.GONE);
			Button b = (Button) findViewById(button);
			b.setBackgroundColor(Color.TRANSPARENT);
		}
		catch (Exception e)
		{
		}
	}

	private void hideLayouts()
	{
		hideLayout(R.id.infodataselayout, R.id.databaseinformation);
		hideLayout(R.id.biosettingslayout, R.id.generalbiometricsettings);
		hideLayout(R.id.optionslayout, R.id.options);
	}

	private void showLayout(int id, int button)
	{
		try
		{
			LinearLayout ll = (LinearLayout) findViewById(id);
			ll.setVisibility(LinearLayout.VISIBLE);
			Button b = (Button) findViewById(button);
			b.setBackgroundColor(Color.WHITE);
		}
		catch (Exception e)
		{
		}
	}

	public void onDatabaseInfoClick(View view)
	{
		hideLayouts();
		showLayout(R.id.infodataselayout, R.id.databaseinformation);
		secondPartTabIndex = 0;
	}

	public void onGeneralBioClick(View view)
	{
		hideLayouts();
		showLayout(R.id.biosettingslayout, R.id.generalbiometricsettings);
		secondPartTabIndex = 1;
	}

	public void onOptionsClick(View view)
	{
		hideLayouts();
		showLayout(R.id.optionslayout, R.id.options);
		secondPartTabIndex = 2;
	}

	public void onImageViewerClick(View view)
	{
		try
		{
			ProcessInfo.getInstance().setImageViewer(((CheckBox) findViewById(R.id.imageviewer)).isChecked());

		}
		catch (Exception e)
		{
		}
	}

	public void onAsyncPositioningCommandClick(View view)
	{
		try
		{
			ProcessInfo.getInstance().setAsyncPositioningCommand(((CheckBox) findViewById(R.id.asyncpositioningcommand)).isChecked());
		}
		catch (Exception e)
		{
		}
	}

	public void onAsyncEnrollmentCommandClick(View view)
	{
		try
		{
			ProcessInfo.getInstance().setAsyncEnrollmentCommand(((CheckBox) findViewById(R.id.asyncenrollmentcommand)).isChecked());
		}
		catch (Exception e)
		{
		}
	}

	public void onAsyncDetectQualityClick(View view)
	{
		try
		{
			ProcessInfo.getInstance().setAsyncDetectQuality(((CheckBox) findViewById(R.id.asyncdetectquality)).isChecked());
		}
		catch (Exception e)
		{
		}
	}

	public void onAsyncCodeQualityClick(View view)
	{
		try
		{
			ProcessInfo.getInstance().setAsyncCodeQuality(((CheckBox) findViewById(R.id.asynccodequality)).isChecked());
		}
		catch (Exception e)
		{
		}
	}

	public void onExportMatchingPkNumberClick(View view)
	{
		try
		{
			ProcessInfo.getInstance().setExportMatchingPkNumber(((CheckBox) findViewById(R.id.asyncdetectquality)).isChecked());
		}
		catch (Exception e)
		{
		}
	}

	public void onWakeUpWithLedOffClick(View view)
	{
		try
		{
			ProcessInfo.getInstance().setWakeUpWithLedOff(((CheckBox) findViewById(R.id.wakeupwithledoff)).isChecked());
		}
		catch (Exception e)
		{
		}
	}

	public void onConfigKeyUser(View view)
	{
		try
		{
			String msg1;
			String msg2;
			byte[] paramValue = new byte[1];
			if(((CheckBox) findViewById(R.id.configkeyuser)).isChecked())
			{
				paramValue[0]=1;
				msg1 = getResources().getString(R.string.alert_setconfig_caution)+"\n"+getResources().getString(R.string.alert_setconfig_reboot) ;
				alert(msg1);
            }
            else
			{
				paramValue[0]=0;
				msg1 = getResources().getString(R.string.alert_setconfig_reboot) ;
				alert(msg1);
			}
            int ret = morphoDevice.setConfigParam(MorphoDevice.CONFIG_KEY_USER_TAG, paramValue);
            if (ret != ErrorCodes.MORPHO_OK)
            {
				msg2 = getResources().getString(R.string.alert_setconfig_error);
				alert(msg2);
			}

		}
		catch (Exception e)
		{
		}
	}
	
	public void onForceFingerPlacementOnTopClick(View view)
	{
		try
		{
			ProcessInfo.getInstance().setForceFingerPlacementOnTop(((CheckBox) findViewById(R.id.forcefingerplacementontop)).isChecked());
		}
		catch (Exception e)
		{
		}
	}

	public void onAdvancedSecLevelCompatibilityReq(View view)
	{
		try
		{
			ProcessInfo.getInstance().setAdvancedSecLevCompReq(((CheckBox) findViewById(R.id.advancedseclevelcompatibilityreq)).isChecked());
		}
		catch (Exception e)
		{
		}
	}

	public void onFingerQualityThresholdClick(View view)
	{
		try
		{
			ProcessInfo.getInstance().setFingerprintQualityThreshold(((CheckBox) findViewById(R.id.fingerqualitythreshold)).isChecked());
			TextView fqtv = (TextView) findViewById(R.id.fingerqualitythresholdvalue);
			fqtv.setEnabled(ProcessInfo.getInstance().isFingerprintQualityThreshold());
		}
		catch (Exception e)
		{
		}
	}

	public void refreshNbrOfUsedRecord()
	{
		//setting number of used records in database
		Long nbUsedRecord = new Long(0);
		morphoDatabase.getNbUsedRecord(nbUsedRecord);
		ProcessInfo.getInstance().setCurrentNumberOfUsedRecordValue(nbUsedRecord);

		try
		{
			TextView curnb = (TextView) findViewById(R.id.currentnumberofusedrecordvalue);
			curnb.setText(Long.toString(ProcessInfo.getInstance().getCurrentNumberOfUsedRecordValue()));
		}
		catch (Exception e)
		{
		}
	}

	public void closeDeviceAndFinishActivity()
	{
		try
		{
			morphoDevice.closeDevice();
		}
		catch (Exception e)
		{
			Log.e("ERROR", e.toString());
		}
		finally
		{
			ProcessInfo.getInstance().setMorphoDevice(null);
			ProcessInfo.getInstance().setMorphoDatabase(null);
			ProcessInfo.getInstance().setDatabaseSelectedIndex(-1);
			
			ArrayList<SecurityOption> securityOptions = ProcessInfo.getInstance().getSecurityOptions();
			for (SecurityOption so : securityOptions) {
				if (so.title.equals("Mode Offered Security") && so.activated) {
					morphoDevice.offeredSecuClose();
				} else if (so.title.equals("Mode Tunneling") && so.activated) {
					morphoDevice.tunnelingClose();
				}
			}
		}
	}

	@Override
	public synchronized void update(Observable observable, Object data)
	{
		Boolean isOpenOK = (Boolean) data;
		
		MorphoSample.isRebootSoft = false;		
		
		if(isOpenOK == true)
		{
			mHandler.post(new Runnable()
			{
				@Override
				public synchronized void run()
				{
					enableDisableBoutton(true);
					// L.B the sample must reload the database after connection resumed
					int ret = morphoDevice.getDatabase(0, morphoDatabase);
					if(ErrorCodes.MORPHO_OK == ret) {
						loadDatabaseItem();
					} else if (ErrorCodes.MORPHOERR_BASE_NOT_FOUND == ret) {
						activateButton(false, false);
					} else {
						alert(ErrorCodes.MORPHOERR_RESUME_CONNEXION,0,"Resume Connection","Failed to reload database.");
					}
				}
			});		
		}
		else
		{
			mHandler.post(new Runnable()
			{
				@Override
				public synchronized void run()
				{
					Button btn = (Button) findViewById(R.id.btn_closeandquit);
					btn.setEnabled(true);
					alert(ErrorCodes.MORPHOERR_RESUME_CONNEXION,0,"Resume Connection","");
				}
			});
		}		
	}
	
	private void enableDisableBoutton(boolean enabled) {
		Button btn = (Button) findViewById(R.id.btn_identitymatch);
		btn.setEnabled(enabled);

		btn = (Button) findViewById(R.id.btn_removeall);
		btn.setEnabled(enabled);

		btn = (Button) findViewById(R.id.btn_removeuser);
		btn.setEnabled(enabled);

		btn = (Button) findViewById(R.id.btn_updateuser);
		btn.setEnabled(enabled);

		btn = (Button) findViewById(R.id.btn_closeandquit);
		btn.setEnabled(enabled);
		
		btn = (Button) findViewById(R.id.btn_createbase);
		btn.setEnabled(enabled);
		
		btn = (Button) findViewById(R.id.btn_destroybase);
		btn.setEnabled(enabled);
		
		btn = (Button) findViewById(R.id.btn_identitymatch);
		btn.setEnabled(enabled);
		
		btn = (Button) findViewById(R.id.btn_adduser);
		btn.setEnabled(enabled);
		
		btn = (Button) findViewById(R.id.btn_verifymatch);
		btn.setEnabled(enabled);
		
		btn = (Button) findViewById(R.id.startstop);
		btn.setEnabled(enabled);
		
		btn = (Button) findViewById(R.id.btn_rebootsoft);
		btn.setEnabled(enabled);
		
		if (menuItemMSOConfiguration != null) {
			menuItemMSOConfiguration.setEnabled(enabled);
		}
		if (menuItemLoggingParameters != null) {
			menuItemLoggingParameters.setEnabled(enabled);
		}
		if (menuItemLoadKsUnsecureMode != null) {
			menuItemLoadKsUnsecureMode.setEnabled(enabled);
		}
		if (menuItemLoadKsAsymetricSecureMode != null) {
			menuItemLoadKsAsymetricSecureMode.setEnabled(enabled);
		}
		if (menuItemLoadKsSymetricSecureMode != null) {
			menuItemLoadKsSymetricSecureMode.setEnabled(enabled);
		}
		if (menuItemGetKs != null) {
			menuItemGetKs.setEnabled(enabled);
		}
		if (menuItemEnableBioDataEncryption != null) {
			menuItemEnableBioDataEncryption.setEnabled(enabled);
		}
		if (menuItemDecryptBioData != null) {
			menuItemDecryptBioData.setEnabled(enabled);
		}
		if (menuItemEncryptAESBioData != null) {
			menuItemEncryptAESBioData.setEnabled(enabled);
		}
		if (menuItemDecryptAESBioData != null) {
			menuItemDecryptAESBioData.setEnabled(enabled);
		}
		/*if (menuItemPrivacyMode != null && menuItemPrivacyMode.isVisible()) {
			menuItemPrivacyMode.setEnabled(enabled);
		}*/
		if (menuItemLoadKprivacyUnsecureMode != null) {
			menuItemDecryptAESBioData.setEnabled(enabled);
		}
		if (menuItemLoadKprivacySymmetricSecureMode != null) {
			menuItemLoadKprivacySymmetricSecureMode.setEnabled(enabled);
		}
		if (menuItemGetprivacyKcv != null) {
			menuItemGetprivacyKcv.setEnabled(enabled);
		}
	}
	
	protected byte[] getPrivacyKey() 
	{
		if (ProcessInfo.getInstance().getPrivacyKey() == null) {
			
			final AlertDialog alertDialog = new AlertDialog.Builder(MorphoSample.this).create();
			alertDialog.setTitle(MorphoSample.this.getResources().getString(R.string.morphosample));
			alertDialog.setMessage(MorphoSample.this.getResources().getString(R.string.selectPrivacyKey));
			alertDialog.setCancelable(false);
			alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					currentAction = MorphoSampleAction.onGetAesKeyLoad;
					try {
						Intent activityIntent = new Intent(MorphoSample.this, FileChooserActivity.class);
						startActivityForResult(activityIntent, 0);
					} catch (Exception e) {
						alert(e.getMessage());
}
				}
			});
			alertDialog.show();
			
			// Wait for event click
			try {
				Looper.loop();
			} catch (RuntimeException e) {
			}
		}
	
		return ProcessInfo.getInstance().getPrivacyKey();		
	}
	
	protected byte[] encryptPrivacyData(byte[] clear_data, byte[] key, byte[] random)
	{
		// Privacy formatted and ciphered data = AES-128.CBC.ENC( Kprivacy, IV, CRC32( RND32 || Plain Data ) || RND32 || Plain Data || Padding )
		// CRC32 computation parameters : Polynomial = 0x04C11DB7, input and output reflected, initial value = 0xFFFFFFFF and final XOR = 0xFFFFFFFF
		byte[] crypted_bio_data=null;
		try {
			// Compute CRC32
			byte[] iv = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
			ArrayList<Byte> aesEncryptedData = new ArrayList<Byte>();
			byte[] datainput = new byte[clear_data.length+ 4] ; //+ size of random
	
			System.arraycopy(random, 0, datainput, 0, 4);			
			System.arraycopy(clear_data,0,datainput, 4, clear_data.length);
						
			long[] computedCRC = {0};
			int ret = msoSecu.computeCRC32(datainput, 0x04C11DB7, 0xFFFFFFFF, true, true, 0xFFFFFFFF, computedCRC);
			if (0 != ret) {
				alert("An error occured while computing CRC32");
				return null;
			}
			
			// Build data to crypt
			byte [] CRC32Buffer= MorphoTools.longToFourByteBuffer(computedCRC[0],false);
			byte[] datainputF = new byte[clear_data.length+4+4] ; //+ size of crc & random
			
			System.arraycopy(CRC32Buffer, 0,datainputF,0, 4);
			System.arraycopy(random, 0,datainputF, 4,4);
			System.arraycopy(clear_data,0,datainputF,8, clear_data.length);
			
			// Crypt data
			ret = msoSecu.encryptAes128Cbc(key, datainputF, iv, true, aesEncryptedData);
			if (ret != 0) {
				alert("An error occured while encrypting data");
				return null;
			} else {
				crypted_bio_data = MorphoTools.toByteArray(aesEncryptedData);
			}
		 } catch (Exception e) {
				alert(e.getMessage());
		 }
		
		return crypted_bio_data;
	}
	
	protected byte[] decryptAndCheckPrivacyData(byte[] cryptedData, byte[] key) {
		int ret = 0;
		
		// Privacy formatted and ciphered data = AES-128.CBC.ENC( Kprivacy, IV, CRC32( RND32 || Plain Data ) || RND32 || Plain Data || Padding )
		// Decrypt data
		byte[] iv = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
		ArrayList<Byte> aesEncryptedData = new ArrayList<Byte>();
		ret = msoSecu.decryptAes128Cbc(key, cryptedData, iv, true, aesEncryptedData);
		if (0 != ret) {
			alert("An error occured while decrypting data");
			return null;
		}
		// No need to delete padding, it is already done by decryptAes128Cbc method
		byte[] decrypteddata = MorphoTools.toByteArray(aesEncryptedData);
				
		// Check CRC32
		byte[] CRC32Buffer = Arrays.copyOf(decrypteddata, 4);
		long CRC32 = MorphoTools.fourBytesToLongValue(CRC32Buffer);
		byte[] dataToCheck = Arrays.copyOfRange(decrypteddata, 4, decrypteddata.length);
		long[] computedCRC = {0};
		ret = msoSecu.computeCRC32(dataToCheck, 0x04C11DB7, 0xFFFFFFFF, true, true, 0xFFFFFFFF, computedCRC);
		if (0 != ret) {
			alert("An error occured while computing CRC32");
			return null;
		}
		
		if (computedCRC[0] != CRC32) {
			alert("CRC32 computation does not match");
			return null;
		}
		
		return Arrays.copyOfRange(dataToCheck, 4, dataToCheck.length);
	}
}
