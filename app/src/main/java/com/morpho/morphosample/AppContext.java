package com.morpho.morphosample;

import android.app.Application;
import android.os.Environment;
import java.util.Random;
import java.io.File;

/**
 * Created by Administrator on 2019/8/28.
 */

public class AppContext extends Application {
    public final static String RootPath = Environment.getExternalStorageDirectory() + File.separator + "logs" + File.separator + "finger_" + new Random().nextInt(100);

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
